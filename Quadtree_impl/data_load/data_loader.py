import numpy as np
import collections
import os
import pdb
import csv
import cv2
import numpy as np
import torch
from qtils import show_examples
import xmltodict
import matplotlib.pyplot as plt
from albumentations import (
    HorizontalFlip,
    VerticalFlip,
    CenterCrop,
    Compose,
    RandomRotate90,
    RandomBrightnessContrast,
    HueSaturationValue
)
from skimage import io
from skimage.color import gray2rgb
from sklearn.model_selection import train_test_split
from torch.utils import data
from torchvision import transforms
import torch
from torchvision import datasets
from torchvision import transforms
from torch.utils.data.sampler import SubsetRandomSampler

class PatternData(data.Dataset):
    def __init__(self, images, masks, labels, i_size, c_size, target='training', sample=False):
        super(PatternData, self).__init__()

        self.masks = masks
        self.images = images
        self.labels = labels

        self.c_size = c_size
        self.i_size = i_size
        self.sample = sample
        self.target = target

        augmentation_list = [
            CenterCrop(height=c_size, width=c_size, always_apply=True),
            RandomBrightnessContrast(brightness_limit=0.05, contrast_limit=0.05,
                                     p=0.5, always_apply=False),
            #HueSaturationValue(hue_shift_limit=5, sat_shift_limit=5, val_shift_limit=5,
            #                   p=0.5, always_apply=False),
            # VerticalFlip(p=0.5),
            HorizontalFlip(p=0.5),
            # RandomRotate90()
        ]

        self.tr_augmentations = Compose(augmentation_list, additional_targets={'automask': 'mask'})
        self.te_augmentations = Compose(augmentation_list[:1])

        self.to_norm_tensor = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])
        ]
        )
        self.to_tensor = transforms.Compose(
            [
                transforms.ToTensor()
            ]
        )

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, item):

        x_image = gray2rgb(io.imread(self.images[item]))
        x_automask = (np.mean(np.array(x_image), axis=2) < 250).astype(np.int)
        x_name = self.images[item].split('/')[-1].split('.')[0]
        x_mask = io.imread(self.masks[item])
        y= self.labels[item]
        if self.sample:
            print('sampling')
            show_examples(x_image[..., 0], y)

        if self.target == 'training':
            augmented = self.tr_augmentations(image=x_image, mask=x_mask, automask=x_automask)
        else:
            augmented = self.te_augmentations(image=x_image, mask=x_mask, automask=x_automask)

        t_im = self.to_norm_tensor(augmented['image'][..., 0])
        t_mask = self.to_tensor(augmented['mask'])
        t_ama = self.to_tensor(augmented['automask'])


        return t_im, (t_mask, t_ama), torch.tensor(y), x_name

def load_pattern_data_train(fname, skip_neg=False, split=False, half_batch=2, limit_n_files=-1):
    """
    Retrieve input filenames and masks from a description file

    :param fname: Description file
    :return: lists with image and mask paths, labels and the blob center positions
    """

    if not os.path.exists(fname):
        raise FileNotFoundError(fname)

    image_paths, mask_paths, labels = list(), list(), list()
    centers = list()

    c_mask = 1
    c_label = 2

    with open(fname, 'r') as ifs:

        for line in ifs.readlines():
            line_data = line.split(', ')

            try:
                label = int(line_data[c_label])
            except ValueError as ve:
                print(line_data)
                continue

            l_split = 1
            if label < l_split:
                label = 0
            else:
                label = 1

            center = 0.5 * (1 + np.array(list(map(float, line_data[3:5]))))
            #print(center)
            image_paths.append(line_data[0].strip('\n'))
            mask_paths.append(line_data[c_mask].strip('\n'))
            labels.append(label)
            centers.append(center)

            # Skip loading
            if len(image_paths) > limit_n_files > 0:
                break
    if not split:
        tr_data = {'training': {
            'images': image_paths,
            'masks': mask_paths,
            'labels': labels,
            'centers': centers
        }, 'testing': None}

        return tr_data
    else:
        testing_group = np.random.choice(
            np.arange(len(labels), dtype=int), size=int(0.1 * len(labels)), replace=False
        )

        tr_te_data = {'training': {
            'images': list(),
            'masks': list(),
            'labels': list(),
            'centers': list()
        }, 'testing': {
            'images': list(),
            'masks': list(),
            'labels': list(),
            'centers': list()
        }}

        prepend = True
        prep_counter = 0
        for i in range(len(labels)):
            target = 'training'
            if i in testing_group:
                target = 'testing'

            if target == 'testing' and prepend and labels[i] > 0:
                tr_te_data[target]['images'] = [image_paths[i]] + tr_te_data[target]['images']
                tr_te_data[target]['masks'] = [mask_paths[i]] + tr_te_data[target]['masks']
                tr_te_data[target]['labels'] = [labels[i]] + tr_te_data[target]['labels']
                tr_te_data[target]['centers'] = [centers[i]] + tr_te_data[target]['centers']

                prep_counter += 1
                if prep_counter > half_batch:
                    prepend = False

            else:
                tr_te_data[target]['images'].append(image_paths[i])
                tr_te_data[target]['masks'].append(mask_paths[i])
                tr_te_data[target]['labels'].append(labels[i])
                tr_te_data[target]['centers'].append(centers[i])

        return tr_te_data

def load_pattern_data_train_val(fname, skip_neg=False, half_batch=2, limit_n_files=-1, shuffle=False,
                                training_split=0.0):
    """
    Retrieve input filenames and masks from a description file

    :param fname: Description file
    :return: lists with image and mask paths, labels and the blob center positions
    """

    if not os.path.exists(fname):
        raise FileNotFoundError(fname)

    tr_num, val_num= 0, 0
    c_mask = 1
    c_label = 2
    lines = np.array([])
    tr_val_data = {}
    with open(fname, 'r') as ifs:
        for line in ifs.readlines():
            lines = np.append(line, lines)
    if shuffle:
        np.random.shuffle(lines)
    assert(training_split != 0.0)
    split_lines = np.array([line.strip().split(', ') for line in lines])
    tra, val = train_test_split(split_lines, stratify=list(map(int, split_lines[:, 2])), test_size=1-training_split, shuffle=True)

    tr_data = tra
    val_data = val
    tr_num = len(tr_data)
    val_num = len(val_data)
    batches = {'train': tr_data, 'val': val_data}
    for batch in ['train', 'val']:
        data_batch = batches[batch]
        image_paths, mask_paths, labels = list(), list(), list()
        centers = list()
        for line in data_batch:
            #line_data = line.split(', ')
            line_data = line
            try:
                label = int(line_data[c_label])
            except ValueError as ve:
                print(line_data)
                continue

            l_split = 1 # 0 - poz. test jde do poz. (lab: +1), 1 - poz. test jde mezi healthy conrol (lab: 0)
            if label < l_split:
                label = 0
            else:
                label = 1

            center = 0.5 * (1 + np.array(list(map(float, line_data[3:5]))))
            #print(center)
            image_paths.append(line_data[0].strip('\n'))
            mask_paths.append(line_data[c_mask].strip('\n'))
            labels.append(label)
            centers.append(center)

            # Skip loading
            if len(image_paths) > limit_n_files > 0:
                break
        if batch == 'train':
            tr_val_data['training'] = {
                    'images': image_paths,
                    'masks': mask_paths,
                    'labels': labels,
                    'centers': centers}
        else:
            tr_val_data['validation'] = {
                'images': image_paths,
                'masks': mask_paths,
                'labels': labels,
                'centers': centers}
    return tr_val_data, tr_num, val_num

def prepare_data(input_description_file, batch_size, image_file_size, initial_size,
                 skip_negative=False, split_validation=True, testing_description_file="None",
                 n_img_limit=-1, sample=False, training_split = 0.0, shuffle=False):
    print("Testing description file")
    tr_num, val_num, test_num = 0, 0, 0
    tr_val_data = {'testing': None}
    if testing_description_file != "None":
        tr_te_data = load_pattern_data_train(input_description_file,
                                       skip_neg=skip_negative, split=False,
                                       half_batch=batch_size // 2, limit_n_files=n_img_limit)

        # Load testing data, then merge dicts
        te_te_data = load_pattern_data_train(testing_description_file,
                                       skip_neg=skip_negative, split=False,
                                       half_batch=batch_size // 2, limit_n_files=n_img_limit)

        tr_te_data['testing'] = te_te_data['training']

    else:
        tr_val_data, tr_num, val_num = load_pattern_data_train_val(input_description_file,
                                       skip_neg=skip_negative,training_split=training_split,
                                       half_batch=batch_size // 2, limit_n_files=n_img_limit, shuffle=shuffle)

    print("Training data: " + "\n".join(tr_val_data['training']['images']))
    tr_data = PatternData(tr_val_data['training']['images'],
                          tr_val_data['training']['masks'],
                          tr_val_data['training']['labels'],
                          image_file_size, initial_size, sample=sample)

    train_loader_params = {
        'batch_size': batch_size,
        'shuffle': False,
        'num_workers': 2
    }

    tr_data_generator = data.DataLoader(tr_data, **train_loader_params)

    print("Validation data: " + "\n".join(tr_val_data['validation']['images']))
    val_data = PatternData(tr_val_data['validation']['images'],
                          tr_val_data['validation']['masks'],
                          tr_val_data['validation']['labels'],
                          image_file_size, initial_size, sample=sample)

    val_loader_params = {
        'batch_size': batch_size,
        'shuffle': False,
        'num_workers': 2
    }

    val_data_generator = data.DataLoader(val_data, **val_loader_params)
    te_data_generator = None
    tr_val_data['testing'] = None
    if tr_val_data['testing'] is not None:
        te_data = PatternData(tr_val_data['testing']['images'],
                              tr_val_data['testing']['masks'],
                              tr_val_data['testing']['labels'],
                              image_file_size, initial_size, target='testing')

        print("=================\nTesting data: " + "\n".join(tr_val_data['testing']['images']))
        test_loader_params = {
            'batch_size': batch_size,
            'shuffle': False,
            #            'num_workers': 1
        }

        te_data_generator = data.DataLoader(te_data, **test_loader_params)
    print("Training model on {} samples, validating on {} samples.".format(
        tr_num, val_num
    ))
    return tr_data_generator, val_data_generator, te_data_generator, tr_num, val_num


def _load_update_dict(current, target, descent_key='root'):
    to_be_updated = target.keys()

    for key, item in target.items():
        # Skip keys if not to be updated
        if key not in to_be_updated:
            continue

        print("Setting {}.{}".format(descent_key, key), end='')
        # Recurse for dict entries, pass string values and eval other value types
        if type(current[key]) is dict and type(target[key]) is collections.OrderedDict:
            print('(...)')
            _load_update_dict(current[key], target[key], descent_key=descent_key + '.' + key)

        elif type(current[key]) == str:
            current[key] = target[key]
            print('--> ', target[key])

        else:
            in_value = eval(target[key])
            current[key] = in_value
            print('--> ', in_value)

    return

def load_run_settings_from_dict(file_name: str, default_values: dict):
    """Update values in default settings dict with provided XML file

    :param file_name: XML file with current values for run parameters
    :param default_values: dictonary with default values (will be updated
    :return: -1 if there were some errors updating the dict, 0 otherwise
    """
    if not os.path.exists(file_name):
        print('Given file ({}) to load not found'.format(file_name))
        return -1

    with open(file_name, 'r') as fh:
        ld_settings = xmltodict.parse(fh.read())
        #print(ld_settings)
    try:
        _load_update_dict(default_values, ld_settings['configuration'])
    except KeyError as ke:
        print('Cannot use given configuration file, exception while parsing occured \n Key not found: ', ke)
        return -1

    return 0

if __name__ == '__main__':
    print('hello')