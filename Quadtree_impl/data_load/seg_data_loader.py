import numpy as np
import collections
import os
import pdb
import csv
import cv2
import numpy as np
import torch
from qtils import show_examples
import xmltodict
import matplotlib.pyplot as plt
from albumentations import (
    HorizontalFlip,
    VerticalFlip,
    CenterCrop,
    Compose,
    RandomRotate90,
    RandomBrightnessContrast,
    HueSaturationValue,
    Resize
)
from skimage import io
from skimage.color import gray2rgb
from sklearn.model_selection import train_test_split
from torch.utils import data
from torchvision import transforms
import torch
from torchvision import datasets
from torchvision import transforms
from torch.utils.data.sampler import SubsetRandomSampler

class PatternData(data.Dataset):
    def __init__(self, images, labels, i_size, c_size, target='training', sample=False):
        super(PatternData, self).__init__()

        self.images = images
        self.labels = labels

        self.c_size = c_size
        self.i_size = i_size
        self.sample = sample
        self.target = target

        augmentation_list = [
            #CenterCrop(height=224, width=224, always_apply=True),
            RandomBrightnessContrast(brightness_limit=0.05, contrast_limit=0.05,
                                     p=0.5, always_apply=False),
            VerticalFlip(p=0.5),
            HorizontalFlip(p=0.5),
            # RandomRotate90()
        ]

        self.tr_augmentations = Compose(augmentation_list)
        self.te_augmentations = Compose(augmentation_list[:1])

        self.to_norm_tensor = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])
        ]
        )
        self.to_tensor = transforms.Compose(
            [
                transforms.ToTensor()
            ]
        )

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, item):

        dir = self.images[item]
        bag = []
        inst_labels = []
        label = []
        if os.path.isfile(os.path.join(dir, 'patch_labels.csv')):
            patches = os.path.join(dir, 'patch_labels.csv')
        else:
            patches = os.path.join(dir, 'patch_labels1.csv')
        with open(patches, 'r', encoding='UTF8') as f:
            csvreader = csv.reader(f)
            for row in csvreader:
                x_image = gray2rgb(io.imread(row[0]))
                #print(np.shape(x_image))
                #x_image = np.transpose(x_image, [3, 2, 0, 1])

                if self.target == 'training':
                    augmented = self.tr_augmentations(image=x_image)
                else:
                    augmented = self.te_augmentations(image=x_image)
                t_im = cv2.resize(augmented['image'][..., 0], (224, 224), interpolation=cv2.INTER_AREA)
                bag.append(np.transpose(t_im, [2, 0, 1]))
                inst_labels.append(int(row[1]))
        label = self.labels[item]
        if self.sample:
            print('sampling')
            show_examples(x_image[..., 0], y)

        return torch.FloatTensor(np.asarray(bag)), torch.tensor(label)

def load_pattern_data_train(fname, skip_neg=False, split=False, half_batch=2, limit_n_files=-1):
    """
    Retrieve input filenames and masks from a description file

    :param fname: Description file
    :return: lists with image and mask paths, labels and the blob center positions
    """

    if not os.path.exists(fname):
        raise FileNotFoundError(fname)

    image_paths, mask_paths, labels = list(), list(), list()
    centers = list()

    c_mask = 1
    c_label = 2

    with open(fname, 'r') as ifs:

        for line in ifs.readlines():
            line_data = line.split(', ')

            try:
                label = int(line_data[c_label])
            except ValueError as ve:
                print(line_data)
                continue


            l_split = 1
            if label < l_split:
                label = 0
            else:
                label = 1

            center = 0.5 * (1 + np.array(list(map(float, line_data[3:5]))))
            #print(center)
            image_paths.append(line_data[0].strip('\n'))
            mask_paths.append(line_data[c_mask].strip('\n'))
            labels.append(label)
            centers.append(center)

            # Skip loading
            if len(image_paths) > limit_n_files > 0:
                break
    if not split:
        tr_data = {'training': {
            'images': image_paths,
            'masks': mask_paths,
            'labels': labels,
            'centers': centers
        }, 'testing': None}

        return tr_data
    else:
        testing_group = np.random.choice(
            np.arange(len(labels), dtype=int), size=int(0.1 * len(labels)), replace=False
        )

        tr_te_data = {'training': {
            'images': list(),
            'masks': list(),
            'labels': list(),
            'centers': list()
        }, 'testing': {
            'images': list(),
            'masks': list(),
            'labels': list(),
            'centers': list()
        }}

        prepend = True
        prep_counter = 0
        for i in range(len(labels)):
            target = 'training'
            if i in testing_group:
                target = 'testing'

            if target == 'testing' and prepend and labels[i] > 0:
                tr_te_data[target]['images'] = [image_paths[i]] + tr_te_data[target]['images']
                tr_te_data[target]['labels'] = [labels[i]] + tr_te_data[target]['labels']
                tr_te_data[target]['centers'] = [centers[i]] + tr_te_data[target]['centers']

                prep_counter += 1
                if prep_counter > half_batch:
                    prepend = False

            else:
                tr_te_data[target]['images'].append(image_paths[i])
                tr_te_data[target]['labels'].append(labels[i])
                tr_te_data[target]['centers'].append(centers[i])

        return tr_te_data

def load_pattern_data_train_val(fname, skip_neg=False, half_batch=2, limit_n_files=-1, shuffle=False,
                                training_split=0.0):
    """
    Retrieve input filenames and masks from a description file

    :param fname: Description file
    :return: lists with image and mask paths, labels and the blob center positions
    """

    if not os.path.exists(fname):
        raise FileNotFoundError(fname)

    tr_num, val_num= 0, 0
    c_label = 1
    lines = np.array([])
    tr_val_data = {}
    tra = []
    val = []
    assert(training_split != 0.0)
    with open(os.path.join(fname, 'patches_paths_train.csv'), 'r', encoding='UTF8') as f:
        csvreader = csv.reader(f)
        for row in csvreader:
            # print(row)
            tra.append(row)

    with open(os.path.join(fname, 'patches_paths_val.csv'), 'r', encoding='UTF8') as f:
        csvreader = csv.reader(f)
        for row in csvreader:
            # print(row)
            val.append(row)

    tr_data = tra
    val_data = val
    tr_num = len(tr_data)
    val_num = len(val_data)
    batches = {'train': tr_data, 'val': val_data}
    for batch in ['train', 'val']:
        data_batch = batches[batch]
        image_paths, labels = list(), list()
        centers = list()
        for line in data_batch:
            #line_data = line.split(', ')
            line_data = line
            try:
                label = int(line_data[c_label])
            except ValueError as ve:
                print(line_data)
                continue
            found = 0
            if os.path.isfile(os.path.join(line[0].strip('\n'), 'patch_labels.csv')):
                found = 1
            elif os.path.isfile(os.path.join(line[0].strip('\n'), 'patch_labels1.csv')):
                found = 1
            l_split = 1 # 0 - poz. test jde do poz. (lab: +1), 1 - poz. test jde mezi healthy conrol (lab: 0)
            if label < l_split:
                label = 0
            else:
                label = 1

            center = 0.5 * (1 + np.array(list(map(float, line_data[3:5]))))
            #print(center)
            image_paths.append(line_data[0].strip('\n'))
            labels.append(label)
            centers.append(center)

        if batch == 'train':
            tr_val_data['training'] = {
                'images': image_paths,
                'labels': labels,
                'centers': centers}
        else:
            tr_val_data['validation'] = {
                'images': image_paths,
                'labels': labels,
                'centers': centers}
    return tr_val_data, tr_num, val_num

def load_pattern_data_test(fname, skip_neg=False, half_batch=2, limit_n_files=-1, shuffle=False,
                                training_split=0.0):
    """
    Retrieve input filenames and masks from a description file

    :param fname: Description file
    :return: lists with image and mask paths, labels and the blob center positions
    """

    if not os.path.exists(fname):
        raise FileNotFoundError(fname)

    tr_num, val_num= 0, 0
    c_label = 1
    lines = np.array([])
    test_data = {}
    tst = []
    assert(training_split != 0.0)

    with open(os.path.join(fname, 'patches_all.csv'), 'r', encoding='UTF8') as f:
        csvreader = csv.reader(f)
        for row in csvreader:
            # print(row)
            tst.append(row)

    tst_num = len(tst)

    data_batch = tst
    image_paths, labels = list(), list()
    centers = list()
    for line in data_batch:
        #line_data = line.split(', ')
        line_data = line
        try:
            label = int(line_data[c_label])
        except ValueError as ve:
            print(line_data)
            continue
        found = 0

        l_split = 1 # 0 - poz. test jde do poz. (lab: +1), 1 - poz. test jde mezi healthy conrol (lab: 0)
        if label < l_split:
            label = 0
        else:
            label = 1
        center = 0.5 * (1 + np.array(list(map(float, line_data[3:5]))))
        #print(center)
        image_paths.append(line_data[0].strip('\n'))
        labels.append(label)
        centers.append(center)


    test_data['testing'] = {
        'images': image_paths,
        'labels': labels,
        'centers': centers}

    return test_data, tst_num

def prepare_data(input_description_file, batch_size, image_file_size, initial_size,
                 skip_negative=False, split_validation=True, testing_description_file='None',
                 n_img_limit=-1, sample=False, training_split = 0.0, shuffle=False):
    print("Testing description file")
    tr_num, val_num, test_num = 0, 0, 0
    test_data = {'testing': None}
    print(testing_description_file)
    if testing_description_file != 'None':
        print("testing")
        tr_val_data, tr_num, val_num = load_pattern_data_train_val(input_description_file,
                                                                   skip_neg=skip_negative,training_split=training_split,
                                                                   half_batch=batch_size // 2, limit_n_files=n_img_limit, shuffle=shuffle)

        # Load testing data, then merge dicts
        test_data, test_num = load_pattern_data_test(testing_description_file,
                                                                   skip_neg=skip_negative,training_split=training_split,
                                                                   half_batch=batch_size // 2, limit_n_files=n_img_limit, shuffle=shuffle)


    else:
        tr_val_data, tr_num, val_num = load_pattern_data_train_val(input_description_file,
                                                                   skip_neg=skip_negative,training_split=training_split,
                                                                   half_batch=batch_size // 2, limit_n_files=n_img_limit, shuffle=shuffle)

    print("Training data: " + "\n".join(tr_val_data['training']['images']))
    tr_data = PatternData(tr_val_data['training']['images'],
                          tr_val_data['training']['labels'],
                          image_file_size, initial_size, sample=sample)

    train_loader_params = {
        'batch_size': batch_size,
        'shuffle': False,
        'num_workers': 2
    }

    tr_data_generator = data.DataLoader(tr_data, **train_loader_params)

    print("Validation data: " + "\n".join(tr_val_data['validation']['images']))
    val_data = PatternData(tr_val_data['validation']['images'],
                           tr_val_data['validation']['labels'],
                           image_file_size, initial_size, sample=sample)

    val_loader_params = {
        'batch_size': batch_size,
        'shuffle': False,
        'num_workers': 2
    }

    val_data_generator = data.DataLoader(val_data, **val_loader_params)
    te_data_generator = None

    if test_data['testing'] is not None:
        te_data = PatternData(test_data['testing']['images'],
                              test_data['testing']['labels'],
                              image_file_size, initial_size, target='testing')

        print("=================\nTesting data: " + "\n".join(test_data['testing']['images']))
        test_loader_params = {
            'batch_size': batch_size,
            'shuffle': False,
            'num_workers': 2
        }

        te_data_generator = data.DataLoader(te_data, **test_loader_params)
    print("Training model on {} samples, validating on {} samples.".format(
        tr_num, val_num
    ))
    return tr_data_generator, val_data_generator, te_data_generator, tr_num, val_num


def _load_update_dict(current, target, descent_key='root'):
    to_be_updated = target.keys()

    for key, item in target.items():
        # Skip keys if not to be updated
        if key not in to_be_updated:
            continue

        print("Setting {}.{}".format(descent_key, key), end='')
        # Recurse for dict entries, pass string values and eval other value types
        if type(current[key]) is dict and type(target[key]) is collections.OrderedDict:
            print('(...)')
            _load_update_dict(current[key], target[key], descent_key=descent_key + '.' + key)

        elif type(current[key]) == str:
            current[key] = target[key]
            print('--> ', target[key])

        else:
            in_value = eval(target[key])
            current[key] = in_value
            print('--> ', in_value)

    return

def load_run_settings_from_dict(file_name: str, default_values: dict):
    """Update values in default settings dict with provided XML file

    :param file_name: XML file with current values for run parameters
    :param default_values: dictonary with default values (will be updated
    :return: -1 if there were some errors updating the dict, 0 otherwise
    """
    if not os.path.exists(file_name):
        print('Given file ({}) to load not found'.format(file_name))
        return -1

    with open(file_name, 'r') as fh:
        ld_settings = xmltodict.parse(fh.read())
        #print(ld_settings)
    try:
        _load_update_dict(default_values, ld_settings['configuration'])
    except KeyError as ke:
        print('Cannot use given configuration file, exception while parsing occured \n Key not found: ', ke)
        return -1

    return 0