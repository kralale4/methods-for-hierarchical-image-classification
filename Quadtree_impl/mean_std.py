import numpy as np
from models import Quadtree_baseline as quad_b
from tensorflow.keras.preprocessing.image import save_img
import qtils

def compute_mean_std(Xbatch, mse_lst):
    for image in Xbatch:
        img = image.numpy()
        imgT = np.transpose(img, [1, 2, 0])
        image = (imgT - np.amin(imgT)) / (np.amax(imgT) - np.amin(imgT))  # to normalize
        image = (image * 255).astype(np.uint8)
        stains, im_stains = quad_b.get_deconv_img(image)
        save_img(('image_test' + '.jpg'), image)
        qtils.plot_grey_images(stains, im_stains)
        hematoxylin = im_stains[:, :, 0]
        h_avg = np.mean(hematoxylin)
        h_mse = np.square(np.subtract(hematoxylin, h_avg)).mean()
        mse_lst.append(h_mse)
        print(h_mse)
    return mse_lst


def get_mean_std(train_gen, val_gen):
    mse_lst = []
    for i, (Xbatch, Mbatch, tYbatch, x_name) in enumerate(train_gen):
        print('images_loaded')
        mse_lst = compute_mean_std(Xbatch, mse_lst)

    for i, (Xbatch, Mbatch, tYbatch, x_name) in enumerate(val_gen):
        print('images_loaded')
        mse_lst = compute_mean_std(Xbatch, mse_lst)

    mse_lst = np.asarray(mse_lst)
    mean = np.mean(mse_lst)
    std = np.std(mse_lst)
    print('mean: {}', mean)
    print('std : {}', std)
    lines = ['mean: ' + str(mean), 'std: ' + str(std)]
    with open('mean_std.txt', 'w') as f:
        f.writelines(lines)