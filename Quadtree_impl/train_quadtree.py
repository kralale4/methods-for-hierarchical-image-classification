import os
import time
import shutil
import torch
import torch.nn as nn
from tqdm import tqdm
from tensorboard_logger import configure, log_value
from torch.utils.tensorboard import SummaryWriter
import numpy as np
import torch.nn.functional as F
import qtils
from models import Quadtree_baseline as qb
from models.attention_mil import Attention, GatedAttention, FeatureExtractor, MIL_fc, resnet50_baseline
from torch.autograd import Variable
from topk.svm import SmoothTop1SVM
from torch.optim.lr_scheduler import ReduceLROnPlateau
from models.clam_model import CLAM_SB
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from numpy import save
import matplotlib.pyplot as plt

num_channels = 1
num_classes = 2

def train_q_model(settings, train_gen, val_gen, test_gen = None):
    """
    Trains the segmentation results from the Quadtree. Uses the train and validation data to train attention MIL model.
    Saves the checkpoints of the model after each epoch.
    :param settings:
    :param train_gen:
    :param val_gen:
    :return:
    """
    if settings['use_gpu'] and torch.cuda.is_available():
        device = torch.device("cuda:1")
    else:
        device = torch.device("cpu")

    if settings['use_gpu'] and torch.cuda.is_available():
        feat_device = torch.device("cuda:0")
    else:
        feat_device = torch.device("cpu")
    print(torch.cuda.device_count())
    print(feat_device)
    if settings['use_tensorboard']:
        model_name = "quad_{}x{}_{}_{}".format(
            settings['deph'],
            settings['patch_size'],
            settings['patch_size'],
            settings['optimisation']
        )
        settings['model_name'] = model_name
        tensorboard_dir = settings['log_path'] + model_name
        print("[*] Saving tensorboard logs to {}".format(tensorboard_dir))
        if not os.path.exists(tensorboard_dir):
            os.makedirs(tensorboard_dir)
        layout = {
            "Quadtree_class": {
                "loss": ["Multiline", ["loss/train", "loss/validation"]],
                "accuracy": ["Multiline", ["accuracy/train", "accuracy/validation"]],
            },
        }
        writer = SummaryWriter()
        writer.add_custom_scalars(layout)

    best_valid_acc = 0.0
    lr_counter = 0
    feat_model = resnet50_baseline(pretrained=True)
    feat_model = feat_model.to(torch.device('cuda'))
    if torch.cuda.device_count() > 1:
        feat_model = nn.DataParallel(feat_model)

    model_dict = {"dropout": True, 'n_classes': 2}
    CLAM = True
    if (CLAM):
        model_dict.update({'k_sample': 8})
        from topk.svm import SmoothTop1SVM
        instance_loss_fn = SmoothTop1SVM(n_classes = 2)
        instance_loss_fn = instance_loss_fn.cuda(0)
        print(instance_loss_fn)
        model = CLAM_SB(**model_dict, instance_loss_fn=instance_loss_fn)
        optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, model.parameters()), lr=settings['init_lr'], weight_decay=settings['weight_decay'])
    else:
        model = MIL_fc(**model_dict)
        optimizer = torch.optim.Adam(model.parameters(), lr=settings['init_lr'], betas=(0.9, 0.999))
    model.relocate()
    scheduler = ReduceLROnPlateau(optimizer, "min", patience=settings['lr_reduction'])
    start_epoch = 0
    if settings['is_test']:
        model, optimizer, best_valid_acc, start_epoch = load_checkpoint(settings, model, optimizer)
        test_loss, test_acc, model, propabs, Ys = test(test_gen, settings, device, model, feat_model)

        if not settings['slide_train']:
            propabs = np.asarray(propabs)
            Ys = np.asarray(Ys)
            auc_score = roc_auc_score(Ys, propabs[:, 1])
            save('probs.npy', propabs)
            save('labels.npy', Ys)
            fpr1, tpr1, thresh1 = roc_curve(Ys, propabs[:,1], pos_label=1)
            plt.style.use('seaborn')
            plt.plot(fpr1, tpr1, linestyle='--',color='orange', label='RVAM')
            # title
            plt.title('ROC curve')
            # x label
            plt.xlabel('False Positive Rate')
            # y label
            plt.ylabel('True Positive rate')

            plt.legend(loc='best')
            plt.savefig('roc_curve.png')
            print("\nTest stats\n"
                  "Average test loss: {}, average test acc: {}, test error: {}, auc score: {}\n".format(
                test_loss, test_acc, 100-test_acc, auc_score
            ))
        return propabs, Ys
    if settings['load']:
        print('loading the most recent state...')
        model, optimizer, best_valid_acc, start_epoch = load_checkpoint(settings, model, optimizer, best=False)
        start_epoch = 0
        best_valid_acc = 0.0
        print('Most recent state loaded')
    if(start_epoch != 0):
        epoch = start_epoch
    for epoch in range(start_epoch, settings['n_epochs'], 1):


        print("\nEpoch  {} / {} with LR: {}.".format(
            epoch, settings['n_epochs'], optimizer.param_groups[0]["lr"]
        ))
        #torch.cuda.empty_cache()
        train_loss, train_acc, optimizer, model, feat_model = \
            train_one_epoch_supervised_clam(epoch,model, feat_model,optimizer, train_gen, settings, device, feat_device, writer=writer)
        val_loss, val_acc, model, feat_model = \
            validate_supervised(epoch, val_gen, settings, device, model, feat_model, writer=writer)

        scheduler.step(-val_acc)
        is_best=(val_acc > best_valid_acc)
        if(val_acc > best_valid_acc):
            #Improvement measured
            lr_counter = 0
            print("\nEpoch stats\n"
                  "train loss: {}, train acc: {}\n"
                  "valid loss: {}, valid acc: {}\n"
                  "best current validation accuracy\n".format(
                train_loss, train_acc, val_loss, 100-val_acc
            ))
        else:
            #Accuracy has not improved
            lr_counter += 1
            print("\nEpoch stats\n"
                  "train loss: {}, train acc: {}\n"
                  "valid loss: {}, valid acc: {}, valid error: {}\n".format(
                train_loss, train_acc, val_loss,val_acc, 100-val_acc
            ))
        print("*****************************")

        best_valid_acc = max(val_acc, best_valid_acc)
        save_checkpoint(
            {
                "epoch": epoch + 1,
                "model_state": model.state_dict(),
                "optim_state": optimizer.state_dict(),
                "best_valid_acc": best_valid_acc,
            },
            is_best,
            settings=settings
        )

def save_checkpoint(state, is_best, settings):
    """Saves a checkpoint of the model.
    If this model has reached the best validation accuracy thus
    far, a seperate file with the suffix `best` is created.
    """
    filename = settings['model_name'] + "_ckpt.pth.tar"
    ckpt_path = os.path.join(settings['ckpnt_dir'], filename)
    torch.save(state, ckpt_path)
    if is_best:
        filename = settings['model_name'] + "_model_best.pth.tar"
        shutil.copyfile(ckpt_path, os.path.join(settings['ckpnt_dir'], filename))

def load_checkpoint(settings, model, optimizer, best=True,):
    print("[*] Loading model from {}".format(settings['ckpnt_dir']))

    filename = settings['model_name'] + "_ckpt.pth.tar"

    if best:
        filename = settings['model_name'] + "_model_best.pth.tar"
    ckpt_path = os.path.join(settings['ckpnt_dir'], filename)
    ckpt_path = '/mnt/medical_temporary/microscopy/kralale4/Quadtree/CKPNTS_1_5/quad_4x244_244_-1.5_model_best.pth.tar'
    ckpt = torch.load(ckpt_path, map_location=torch.device("cuda:0"))
    print('loaded: ' + ckpt_path)
    # load variables from checkpoint
    start_epoch = ckpt["epoch"]
    best_valid_acc = ckpt["best_valid_acc"]
    model.load_state_dict(ckpt["model_state"])
    optimizer.load_state_dict(ckpt["optim_state"])

    if best:
        print(
            "[*] Loaded {} checkpoint @ epoch {} "
            "with best valid acc of {:.3f}".format(
                filename, ckpt["epoch"], ckpt["best_valid_acc"]
            )
        )
    else:
        print("[*] Loaded {} checkpoint @ epoch {}".format(filename, ckpt["epoch"]))
    return model, optimizer, best_valid_acc, start_epoch

def train_one_epoch_supervised(epoch, model, feat_model, optimizer, train_gen, settings, device, feat_device):
    losses = 0.0
    model.train()
    feat_model.eval()
    accs = 0.0
    start_time = time.time()
    loss_count = 0
    acc_count = 0
    slide_criterion = nn.CrossEntropyLoss()
    with tqdm(total=settings['num_train']) as pbar:
        for i, (Xbag, Ybatch) in enumerate(train_gen):
            #print(Ybatch)
            batch_size = np.shape(Xbag)[0]
            data, bag_label = Xbag.to(torch.device('cuda')), Ybatch.to(device)

            optimizer.zero_grad()
            features = feat_model(data[0])
            features = features.to(device)
            logits, Y_probs, Y_hat, _, _= model(features)

            correct = (Y_hat.to(device) == bag_label).float()
            acc = 100 * (correct.sum() / len(bag_label))
            loss = slide_criterion(logits, bag_label)
            losses += loss.item()*batch_size
            accs += acc*batch_size
            loss_count += batch_size
            acc_count += batch_size

            # compute gradients and update SGD
            loss.backward()
            optimizer.step()

            end_time = time.time()
            # measure elapsed time
            pbar.set_description(
                (
                    "{:.1f}s - loss: {:.3f} - acc: {:.3f} - error: {:3f}".format(
                        (end_time - start_time), loss.item(), acc, 100-acc
                    )
                )
            )
            pbar.update(batch_size)
            [g.cpu().data.numpy().squeeze() for g in data[0]]

        # log to tensorboard
            if settings['use_tensorboard']:
                loss_avg = losses / loss_count
                acc_avg = accs / acc_count
                iteration = epoch * len(train_gen) + i
                writer.add_scalar("loss/train", loss_avg, iteration)
                writer.add_scalar("accuracy/train", acc_avg, iteration)
        loss_avg = losses / loss_count
        acc_avg = accs / acc_count
        return loss_avg, acc_avg, optimizer, model, feat_model

def train_one_epoch_supervised_clam(epoch, model, feat_model, optimizer, train_gen, settings, device, feat_device, writer=None):
    losses = 0.0
    model.train()
    feat_model.eval()
    accs = 0.0
    start_time = time.time()
    #batch_size = settings['batch_size']
    loss_count = 0
    acc_count = 0
    slide_criterion = SmoothTop1SVM(n_classes = 2)
    slide_criterion = slide_criterion.cuda(0)
    with tqdm(total=settings['num_train']) as pbar:
        for i, (Xbag, Ybatch) in enumerate(train_gen):
            #print(Ybatch)
            batch_size = np.shape(Xbag)[0]
            if np.shape(Xbag)[1] < 8:
                k = int(np.shape(Xbag)[1]/2)
            else:
                k=0

            data, bag_label = Xbag.to(torch.device('cuda')), Ybatch.to(device)

            optimizer.zero_grad()
            features = feat_model(data[0])
            features = features.to(device)
            logits, Y_prob, Y_hat, _, instance_dict = model(features, label=bag_label, instance_eval=True, k=k)
            slide_loss = slide_criterion(logits, bag_label)
            instance_loss = instance_dict['instance_loss']
            loss = 0.7 * slide_loss + (1-0.7) * instance_loss
            correct = (Y_hat.to(device) == bag_label).float()
            acc = 100 * (correct.sum() / len(bag_label))
            losses += loss.item()*batch_size
            accs += acc*batch_size
            loss_count += batch_size
            acc_count += batch_size

            # compute gradients and update SGD
            loss.backward()
            optimizer.step()
            end_time = time.time()
            # measure elapsed time
            pbar.set_description(
                (
                    "{:.1f}s - loss: {:.3f} - acc: {:.3f} - error: {:3f}".format(
                        (end_time - start_time), loss.item(), acc, 100-acc
                    )
                )
            )
            pbar.update(batch_size)
            [g.cpu().data.numpy().squeeze() for g in data[0]]

            if settings['use_tensorboard']:
                loss_avg = losses / loss_count
                acc_avg = accs / acc_count
                iteration = epoch * len(train_gen) + i
                writer.add_scalar("loss/train", loss_avg, iteration)
                writer.add_scalar("accuracy/train", acc_avg, iteration)
        loss_avg = losses / loss_count
        acc_avg = accs / acc_count
        return loss_avg, acc_avg, optimizer, model, feat_model

@torch.no_grad()
def validate_supervised(epoch, val_gen, settings, device, model, feat_model, writer=None):
    losses = 0.0

    accs = 0.0
    loss_count = 0
    acc_count = 0
    feat_model.eval()
    slide_criterion = SmoothTop1SVM(n_classes = 2)
    slide_criterion = slide_criterion.cuda(0)
    #slide_criterion = nn.CrossEntropyLoss(weight=weights_s)
    for i, (Xbag, Ybatch) in enumerate(val_gen):
        batch_size = np.shape(Xbag)[0]
        if np.shape(Xbag)[1] < 8:
            k = int(np.shape(Xbag)[1]/2)
            print("k smaller")
        else:
            k=0
        plot = True
        imgs = []
        #print(bag_label)
        print('images_loaded')
        data, bag_label = Xbag.to(torch.device('cuda')), Ybatch.to(device)
        print(np.shape(data))
        print(np.shape(bag_label))
        features = feat_model(data[0])
        features = features.to(feat_device)
        logits, Y_prob, Y_hat, _, instance_dict= model(features, label=bag_label, instance_eval=True, k=k)
        correct = (Y_hat.to(device) == bag_label).float()
        acc = 100 * (correct.sum() / len(bag_label))
        print(logits)
        print(bag_label)
        slide_loss = slide_criterion(logits, bag_label)
        instance_loss = instance_dict['instance_loss']
        loss = 0.7 * slide_loss + (1-0.7) * instance_loss
        losses += loss.item()*batch_size
        accs += acc*batch_size
        loss_count += batch_size
        acc_count += batch_size

        if settings['use_tensorboard']:
            loss_avg = losses / loss_count
            acc_avg = accs / acc_count
            iteration = epoch * len(val_gen) + i
            writer.add_scalar("loss/validation", loss_avg, iteration)
            writer.add_scalar("accuracy/validation", acc_avg, iteration)
    loss_avg = losses / loss_count
    acc_avg = accs / acc_count
    return loss_avg, acc_avg, model, feat_model

@torch.no_grad()
def test(test_gen, settings, device, model, feat_model, writer=None):
    losses = 0.0

    accs = 0.0
    loss_count = 0
    acc_count = 0
    feat_model.eval()
    slide_criterion = SmoothTop1SVM(n_classes = 2)
    slide_criterion = slide_criterion.cuda(0)
    probs = []
    yss = []
    for i, (Xbag, Ybatch) in enumerate(test_gen):
        batch_size = np.shape(Xbag)[0]
        if np.shape(Xbag)[1] < 8:
            k = int(np.shape(Xbag)[1]/2)
        else:
            k=0
        data, bag_label = Xbag.to(torch.device('cuda')), Ybatch.to('cuda')
        features = feat_model(data[0])
        logits, Y_prob, Y_hat, _, instance_dict= model(features, label=bag_label, instance_eval=True, k=k)
        correct = (Y_hat == bag_label).float()
        acc = 100 * (correct.sum() / len(bag_label))
        probs = [*probs, *(F.softmax(logits).detach().to('cpu').tolist())]
        yss = [*yss, *(bag_label.detach().to('cpu').tolist())]
        slide_loss = slide_criterion(logits, bag_label)
        instance_loss = instance_dict['instance_loss']
        loss = 0.7 * slide_loss + (1-0.7) * instance_loss
        losses += loss.item()*batch_size
        accs += acc*batch_size
        loss_count += batch_size
        acc_count += batch_size

    loss_avg = losses / loss_count
    acc_avg = accs / acc_count
    return loss_avg, acc_avg, model, probs, yss
