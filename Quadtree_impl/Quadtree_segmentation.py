import os
import time
import shutil
import torch
import torch.nn as nn
from tqdm import tqdm
import csv
from tensorboard_logger import configure, log_value
import numpy as np
from models import Quadtree_baseline as qb
from tensorflow.keras.preprocessing.image import save_img
import qtils
num_channels = 1
num_classes = 2


def segment_q_model(settings, train_gen, val_gen):

    threshold = settings['mean'] + settings['optimisation'] * settings['std']
    print('\nSplitting for mean: {} \n std: {} \n {} {} {} = {}'.format(
        settings['mean'], settings['std'], settings['mean'], settings['optimisation'], settings['std'], threshold))

    threshold = settings['mean'] + settings['optimisation'] * settings['std']
    print(threshold)
    start_time = time.time()
    counter = 0
    seg_out_dir = settings['segmentation_results']
    with open(os.path.join(seg_out_dir, 'patches_paths_train.csv'), 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        header = ['path', 'label']
        writer.writerow(header)
        with tqdm(total=settings['num_train']) as pbar:
            for i, (Xbatch, Mbatch, tYbatch, x_name) in enumerate(train_gen):
                print(x_name)
                batch_size = Xbatch.size()[0]
                print(tYbatch)
                print('images_loaded')
                patches = 0
                for j, image in enumerate(Xbatch):
                    Y = tYbatch[j].item()
                    name = x_name[j]
                    path_to_patch_folder = os.path.join(seg_out_dir, name)
                    save_to_csv(writer, path_to_patch_folder, Y)
                    if not os.path.isdir(path_to_patch_folder):
                        os.mkdir(path_to_patch_folder)
                    if os.path.isdir(path_to_patch_folder):
                        print('Tumor file already segmented, skipping...')
                        counter += 1
                        continue
                    else:
                        os.mkdir(path_to_patch_folder)
                    children = qb.segmentQuadTree(image.numpy(), threshold=threshold, img_boarder=20, line_color=(0, 0, 0),
                                                  line_boarder=5)

                    with open(os.path.join(path_to_patch_folder, 'patch_labels.csv'), 'w', encoding='UTF8', newline='') as P:
                        patch_writer = csv.writer(P)
                        #mask = Mbatch[0][j]
                        for i, child in enumerate(children):
                            relative = str(counter)+ '_' + str(i)
                            full_path = os.path.join(path_to_patch_folder, 'patch_' + relative + '.jpg')
                            # patch_label = qtils.get_patch_label(mask,
                            #                                     start=[child.x0, child.y0],
                            #                                     end=[child.x0+child.width, child.y0+child.height],
                            #                                     Ybatch=Y)
                            #patch label not used
                            patch_label = 0
                            save_to_csv(patch_writer,full_path,patch_label)
                            child_img = child.int_img
                            save_img(full_path, child_img)
                    counter += 1

                    patches += len(children)
                end_time = time.time()
                pbar.set_description(
                    (
                        "{:.1f}s - Patches segmented: {} in train".format(
                            (end_time - start_time), patches
                        )
                    )
                )
                pbar.update(batch_size)
        f.close()
    counter = 0
    with open(os.path.join(seg_out_dir, 'patches_paths_val.csv'), 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        header = ['path', 'label']
        writer.writerow(header)
        with tqdm(total=settings['num_valid']) as pbar:
            for i, (Xbatch, Mbatch, tYbatch, x_name) in enumerate(val_gen):

                # utils.save_tensor_images('mask', mask)
                batch_size = Xbatch.size()[0]
                print(tYbatch)
                print('images_loaded')
                patches = 0
                for j, image in enumerate(Xbatch):
                    Y = tYbatch[j].item()
                    name = x_name[j]
                    path_to_patch_folder = os.path.join(seg_out_dir, name)
                    save_to_csv(writer, path_to_patch_folder, Y)
                    if not os.path.isdir(path_to_patch_folder):
                        os.mkdir(path_to_patch_folder)
                    if os.path.isdir(path_to_patch_folder):
                        print('Tumor file already segmented, skipping...')
                        counter += 1
                        continue
                    else:
                        os.mkdir(path_to_patch_folder)
                    children = qb.segmentQuadTree(image.numpy(), threshold=threshold, img_boarder=20, line_color=(0, 0, 0),
                                                  line_boarder=5)

                    with open(os.path.join(path_to_patch_folder, 'patch_labels.csv'), 'w', encoding='UTF8', newline='') as P:
                        patch_writer = csv.writer(P)
                        #mask = Mbatch[0][j]
                        for i, child in enumerate(children):
                            relative = str(counter)+ '_' + str(i)
                            full_path = os.path.join(path_to_patch_folder, 'patch_' + relative + '.jpg')
                            # patch_label = qtils.get_patch_label(mask,
                            #                                     start=[child.x0, child.y0],
                            #                                     end=[child.x0+child.width, child.y0+child.height],
                            #                                     Ybatch=Y)
                            #patch_label not usee
                            patch_label = 0
                            save_to_csv(patch_writer,full_path,patch_label)
                            child_img = child.int_img
                            save_img(full_path, child_img)

                    counter += 1
                    patches += len(children)
                end_time = time.time()
                pbar.set_description(
                    (
                        "{:.1f}s - Patches segmented: {} in validation".format(
                            (end_time - start_time), patches
                        )
                    )
                )
                pbar.update(batch_size)


def save_to_csv(writer, path, Y):
    writer.writerow([path, Y])
