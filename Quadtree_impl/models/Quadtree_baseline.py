# Open cv library
import cv2

# matplotlib for displaying the images
import skimage
from matplotlib import pyplot as plt
import matplotlib.patches as patches
import matplotlib.image as im
import random
import qtils
import math
import numpy as np
from tensorflow.keras.preprocessing.image import save_img
import histomicstk as htk


def save_im(file_name, qimg, img):
    fig = plt.figure(figsize=(20, 20))
    rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    qrgb = cv2.cvtColor(qimg, cv2.COLOR_BGR2RGB)
    # print(qimg.shape)
    # print(img.shape)
    # print(rgb)
    save_img((file_name + '_q.jpg'), qrgb)
    save_img((file_name + '.jpg'), rgb)


class Node:
    def __init__(self, x0, y0, w, h):
        self.x0 = x0
        self.y0 = y0
        self.width = w
        self.height = h
        self.children = []
        self.int_img = None

    def get_width(self):
        return self.width

    def get_height(self):
        return self.height

    # def get_points(self):
    #     return self.points

    def get_points(self, img):
        return img[self.x0:self.x0 + self.get_width(), self.y0:self.y0 + self.get_height()]

    def get_error(self, img):
        pixels = self.get_points(img)
        pixels = (pixels - np.amin(pixels)) / (np.amax(pixels) - np.amin(pixels))  # to normalize
        pixels = (pixels * 255).astype(np.uint8)
        _, im_stains = get_deconv_img(pixels)
        hematoxylin = im_stains[:, :, 0]
        h_avg = np.mean(hematoxylin)
        h_mse = np.square(np.subtract(hematoxylin, h_avg)).mean()

        return h_mse

    def set_int_image(self, img):
        img = self.get_points(img)
        self.int_img = cv2.resize(img, (244, 244), interpolation=cv2.INTER_AREA)


class QTree():
    def __init__(self, stdThreshold, minPixelSize, img):
        self.threshold = stdThreshold
        self.min_size = minPixelSize
        self.minPixelSize = minPixelSize
        self.img = img
        self.root = Node(0, 0, img.shape[0], img.shape[1])

    # def get_points(self):
    #     return img[self.root.x0:self.root.x0 + self.root.get_width(), self.root.y0:self.root.y0+self.root.get_height()]

    def subdivide(self):
        recursive_subdivide(self.root, self.threshold, self.minPixelSize, self.img)

    def render_img(self, thickness=1, color=(0, 0, 255)):
        imgc = self.img.copy()
        c = find_children(self.root)

        if thickness > 0:
            for n in c:
                # Draw a rectangle
                imgc = cv2.rectangle(imgc, (n.y0, n.x0), (n.y0 + n.get_height(), n.x0 + n.get_width()), color=color,
                                     thickness=thickness)
        return imgc


def displayQuadTree(img, threshold=0.3, minCell=224, img_boarder=20, line_boarder=1, line_color=(0, 0, 0)):
    imgT = np.transpose(img, [1, 2, 0])

    qt = QTree(threshold, minCell, imgT)
    qt.subdivide()
    print(np.shape(qt.root.children))
    img_name = 'rendered_tile'
    qtImg = qt.render_img(thickness=line_boarder, color=line_color)
    file_name = "output/" + img_name
    save_im(file_name, qtImg, imgT)


def get_deconv_img(img):
    im_input = img[:, :, :3]
    im_reference = img[:, :, :3]
    mean_ref, std_ref = htk.preprocessing.color_conversion.lab_mean_std(im_reference)
    I_0 = 255

    stain_1 = 'hematoxylin'  # nuclei stain
    stain_2 = 'eosin'  # cytoplasm stain
    stain_3 = 'null'  # input contains only H&E stains
    stains = ['hematoxylin', 'eosin']
    # stainColorMap = {
    #     'hematoxylin': [0.65, 0.72, 0.0],
    #     'eosin': [0.704, 0.990, 0.0],
    #     'null': [0.286, 0.105, 0.0]
    # }
    stainColorMap = {
        'hematoxylin': [0.65, 0.7, 0.0],
        'eosin': [0.704, 0.990, 0.0],
        'null': [0.2, 0.105, 0.0]
    }
    W = np.array([stainColorMap[stain_1],
                  stainColorMap[stain_2],
                  stainColorMap[stain_3]])
    im_stains = htk.preprocessing.color_deconvolution.color_deconvolution(img, W).Stains

    return stains, im_stains


def recursive_subdivide(node, k, minPixelSize, img):
    #print(node.get_error(img))
    node.set_int_image(img)
    if node.get_error(img) <= k and img.shape[0]!=node.width:
        return
    w_1 = int(math.floor(node.width / 2))
    w_2 = int(math.ceil(node.width / 2))
    h_1 = int(math.floor(node.height / 2))
    h_2 = int(math.ceil(node.height / 2))

    if w_1 <= minPixelSize or h_1 <= minPixelSize:
        #print('leaf')
        return
    x1 = Node(node.x0, node.y0, w_1, h_1)  # top left
    recursive_subdivide(x1, k, minPixelSize, img)

    x2 = Node(node.x0, node.y0 + h_1, w_1, h_2)  # btm left
    recursive_subdivide(x2, k, minPixelSize, img)

    x3 = Node(node.x0 + w_1, node.y0, w_2, h_1)  # top right
    recursive_subdivide(x3, k, minPixelSize, img)

    x4 = Node(node.x0 + w_1, node.y0 + h_1, w_2, h_2)  # btm right
    recursive_subdivide(x4, k, minPixelSize, img)

    node.children = [x1, x2, x3, x4]


def find_children(node):
    if not node.children:
        return [node]
    else:
        children = []
        for child in node.children:
            children += (find_children(child))
    return children


def segmentQuadTree(img, threshold, minCell=224, img_boarder=20, line_boarder=1, line_color=(0, 0, 0)):
    imgT = np.transpose(img, [1, 2, 0])
    qt = QTree(threshold, minCell, imgT)
    qt.subdivide()
    children = find_children(qt.root)
    return children
