import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.preprocessing.image import save_img
import torch
from kornia.utils.draw import draw_rectangle
from torch import nn

pics_dir = '/mnt/home.stud/kralale4/BKP2022/Recurrent_visual_attention_exploration/pics/'

def show_examples(images, labels):
    print(np.shape(images))
    X = images.numpy()
    print(np.shape(X))
    X = np.transpose(X, [0, 2, 3, 1])
    plot_images(X, labels)

def plot_images(images, gd_truth, N0=1):
    """
    :param images: loaded images to be shown
    :param gd_truth: ground truth label of each image
    :param N0: number of images to be shown
    :return:
    """


    fig, axis = plt.subplots(2, 2)
    for i, ax in enumerate(axis.flat):
        print(i)
        ax.imshow(images[i], cmap="Greys_r")
        label = "{}".format(gd_truth[i])
        ax.set_xlabel(label)
        ax.set_xticks([])
        ax.set_yticks([])
        # if i == N0:
        #     break
    print('saving plot')
    plt.savefig('sample.png')

def plot_grey_images(stains, images):
    plt.rcParams['figure.figsize'] = 15, 15
    plt.rcParams['image.cmap'] = 'gray'
    titlesize = 24

    for i in 0, 1:
        plt.figure()
        plt.imshow(images[:, :, i])
        _ = plt.title(stains[i], fontsize=titlesize)
        plt.savefig(stains[i] + '.png')

def save_tensor_images(f_name, images):
    images = images.to(torch.device("cpu"))
    for i in range(len(images)):
        X = images[i].permute(1, 2, 0)
        X = X.numpy()
        save_img((pics_dir + f_name+str(i)+'.png'), X)

def save_tensor_image(f_name, image):
    image = image.to(torch.device("cpu"))
    X = image.permute(1, 2, 0)
    X = X.numpy()
    save_img((f_name + '.png'), X)

def count_black_white(mask):
    print(mask.shape)
    mask = mask.permute(1, 2, 0)
    mask = mask.numpy()
    sought = [1,1,1]
    white  = np.count_nonzero(np.all(mask==sought,axis=2))
    print(f"white: {white}")

    # Count black pixels
    sought = [0,0,0]
    black  = np.count_nonzero(np.all(mask==sought,axis=2))
    print(f"black: {black}")
    return white, black

def initialize_weights(module):
    for m in module.modules():
        if isinstance(m, nn.Linear):
            nn.init.xavier_normal_(m.weight)
            m.bias.data.zero_()

        elif isinstance(m, nn.BatchNorm1d):
            nn.init.constant_(m.weight, 1)
            nn.init.constant_(m.bias, 0)

def get_patch_label(mask, start, end, Ybatch):
    C, H, W = mask.shape
    #TODO
    # tune the threshold value
    threshold = 0.85
    yt = []

    patch = mask[ :, start[1] : end[1], start[0] : end[0]]
    C, H, W = patch.shape
    print(patch.shape)
    white, black = count_black_white(patch)
    white = white/(H*H)
    black = black/(H*H)
    y = 0
    if(white > threshold):
        y = 1
    elif black > threshold:
        y = 0
    else:
        y = int(Ybatch)

    return y

def show_extracted_patches(images, start, end):
    color = torch.tensor([255, 0, 0])
    x, y = torch.tensor([6, 4]), torch.tensor([1, 4])
    batch_n = np.shape(images)[0]
    rects = torch.tensor([[[start[0, 1], start[0, 0], end[0, 1], end[0, 0]]]])
    for i in range(1, batch_n):
        rect = torch.tensor([[[start[i, 1], start[i, 0], end[i, 1], end[i, 0]]]])
        rects = torch.cat((rects, rect), 0)
    images_with_rect = draw_rectangle(images, rects)
    save_tensor_images('tile_with_rect', images_with_rect)
