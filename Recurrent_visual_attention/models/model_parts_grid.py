import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.transforms as T
import numpy as np
import torchvision.models as tvm
from torchvision.models.inception import InceptionE

import utils
from utils import save_tensor_image as sv
from utils import show_extracted_patches as show_patch
from torch.distributions import Normal
import torch.utils.model_zoo as model_zoo


__all__ = ['ResNet', 'resnet18', 'resnet34', 'resnet50', 'resnet101',
           'resnet152']

model_urls = {
    'resnet18': 'https://download.pytorch.org/models/resnet18-5c106cde.pth',
    'resnet34': 'https://download.pytorch.org/models/resnet34-333f7ec4.pth',
    'resnet50': 'https://download.pytorch.org/models/resnet50-19c8e357.pth',
    'resnet101': 'https://download.pytorch.org/models/resnet101-5d3b4d8f.pth',
    'resnet152': 'https://download.pytorch.org/models/resnet152-b121ed2d.pth',
}

class Spatial_attention:
    """A spatial attention mechanism.
    Extracts a foveated glimpse `phi` around location `l`
    from an image `x`.
    Concretely, encodes the region around `l` at a
    high-resolution but uses a progressively lower
    resolution for pixels further from `l`, resulting
    in a compressed representation of the original
    image `x`.
    Args:
        x: a 4D Tensor of shape (B, H, W, C). The minibatch
            of images.
        l: a 2D Tensor of shape (B, 2). Contains normalized
            coordinates in the range [-1, 1].
        g: size of the first square patch.
        k: number of patches to extract in the glimpse.
        s: scaling factor that controls the size of
            successive patches.
    Returns:
        phi: a 5D tensor of shape (B, k, g, g, C). The
            foveated glimpse of the image.
    """

    def __init__(self, g, k, s, glimpses, show=False):
        self.g = g
        self.k = k
        self.s = s
        self.glimpses = glimpses
        self.show = show
    def foveate(self, x, l):
        """Extract `k` square patches of size `g`, centered
        at location `l`. The initial patch is a square of
        size `g`, and each subsequent patch is a square
        whose side is `s` times the size of the previous
        patch.
        The `k` patches are finally resized to (g, g) and
        concatenated into a tensor of shape (B, k, g, g, C).
        """
        phi = []
        size = self.g
        default_size = x.shape[2]
        #print(l)
        size = l[:, [2]]
        loc = l[:, [0, 1]]
        #print(size)
        patch, start, end = self.extract_patch(x, loc, size)
        phi.append(patch)

        phi_default = phi[0]

        phi = torch.cat(phi, 1)
        #sv('glimpse_sample', phi)
        phi = phi.view(phi.shape[0], -1)

        return phi, start, end, phi_default

    def extract_patch(self, x, l, size):
        """Extract a single patch for each image in `x`.
        Args:
        x: a 4D Tensor of shape (B, H, W, C). The minibatch
            of images.
        l: a 2D Tensor of shape (B, 2).
        size: a scalar defining the size of the extracted patch.
        Returns:
            patch: a 4D Tensor of shape (B, size, size, C)
        """
        B, C, H, W = x.shape
        #size = size.type(torch.int64)
        size = self.denormalize(H, size)
        size = size.type(torch.int64)

        for i, value in enumerate(size):
            if(value.item() < self.g):
                size[i] = self.g
        start = self.denormalize(H, l)
        start = start - (size/2)
        end = start + size
        start = start.type(torch.int64)
        end = end.type(torch.int64)
        if(self.show):
            print('**************************************')
            print('Saving plots with extracted patches...')
            show_patch(x,start,end)
            print('Done!')
        # loop through mini-batch and extract patches

        patches = []
        for i in range(B):
            #print(start[i, 0]+size[i])
            if(end[i, 0] > H):
                start[i, 0] -= H-end[i, 0]
                end[i, 0] -= H-end[i, 0]
            if(end[i, 1] > H):
                start[i, 1] -=H-end[i, 1]
                end[i, 1] -= H-end[i, 1]
            if(start[i, 0] < 0):
                start[i, 0] = 0
                end[i, 0] = size[i][0]

            if(start[i, 1] < 0):
                start[i, 1] = 0
                end[i, 1] = size[i][0]

            patch = x[i, :, start[i, 1] : end[i, 1], start[i, 0] : end[i, 0]]

            patches.append(patch)
        transform = T.Resize(self.g)
        for i, patch in enumerate(patches):
            k = size[i].item() // self.g
            patches[i] = transform(F.avg_pool2d(patch, k))
            name = 'patch' + '_' + str(i)
            utils.save_tensor_image(name, patches[i])
        return torch.stack(patches), start, end

    def denormalize(self, T, coords):
        """Convert coordinates in the range [-1, 1] to
        coordinates in the range [0, T] where `T` is
        the size of the image.
        """
        return (0.5 * ((coords + 1.0) * T)).long()

    def exceeds(self, from_x, to_x, from_y, to_y, T):
        """Check whether the extracted patch will exceed
        the boundaries of the image of size `T`.
        """
        if (from_x < 0) or (from_y < 0) or (to_x > T) or (to_y > T):
            return True
        return False

def load_pretrained_weights(model, name):
    pretrained_dict = model_zoo.load_url(model_urls[name])
    model.load_state_dict(pretrained_dict, strict=False)
    return model

class Bottleneck_Baseline(nn.Module):
    expansion = 4

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(Bottleneck_Baseline, self).__init__()
        self.conv1 = nn.Conv2d(inplanes, planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=stride,
                               padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = nn.Conv2d(planes, planes * self.expansion, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(planes * self.expansion)
        self.relu = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out

class ResNet_Baseline(nn.Module):

    def __init__(self, block, layers):
        self.inplanes = 64
        super(ResNet_Baseline, self).__init__()
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0])
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2)
        self.avgpool = nn.AdaptiveAvgPool2d(1)
        self.fcn = nn.Linear(1024, 256)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fcn(x)
        return x


def resnet50_baseline(pretrained=False):
    """Constructs a Modified ResNet-50 model.
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = ResNet_Baseline(Bottleneck_Baseline, [3, 4, 6, 3])
    if pretrained:
        model = load_pretrained_weights(model, 'resnet50')
    return model

class SpatialNetwork(nn.Module):
    def __init__(self, h_g, h_l, g, k, s, c, glimpses, std):
        super().__init__()
        D_in = 27040
        self.std = std
        self.g = g
        self.once = 0
        pretrained = True
        self.pretrained_CNN = ResNet_Baseline(Bottleneck_Baseline, [3, 4, 6, 3])
        if pretrained:
            self.pretrained_CNN = load_pretrained_weights(self.pretrained_CNN, 'resnet50')
        # glimpse layer
        D_in = k * g * g * 3

        self.fc1 = nn.Linear(D_in, h_g)
        # location layer
        D_in = 3
        self.fc2 = nn.Linear(D_in, h_g+h_l)


    # def test(self):
    #     disp_field = torch.FloatTensor(1,2,16,16).normal_(1, 1).requires_grad_()
    #     id_grid = torch.nn.functional.affine_grid(torch.FloatTensor([[[1, 0, 0],[0, 1, 0]]]), size=(1,2,16,16))
    #     print(disp_field.shape)
    #     print(id_grid.shape)

    def forward(self, x, l_t_prev, step_size):

        id_initial_M = torch.zeros((x.shape[0], 2, 3))
        id_initial_M[:, 0, 0] = l_t_prev[:, 0]
        id_initial_M[:, 1, 1] = l_t_prev[:, 0]
        id_initial_M[:, 0, 2] = l_t_prev[:, 1]
        id_initial_M[:, 1, 2] = l_t_prev[:, 2]

        id_grid = F.affine_grid(id_initial_M, [x.shape[0], 1, self.g, self.g])
        tr_grid = id_grid.to('cuda:0')
        xp = F.grid_sample(x, tr_grid)
        l_t_prev = l_t_prev.view(l_t_prev.size(0), -1)
        transform = T.Resize(self.g)
        xp = transform(xp)
        # name = 'patch_after_sample' + '_' + str(step_size)
        # utils.save_tensor_images(name, xp.detach())

        where = self.fc2(l_t_prev)
        what = self.pretrained_CNN(xp)
        g_t = torch.relu(what*where)
        start=0
        end=0
        return g_t, what, start, end

class Appearance_Spatial_network_CNN(nn.Module):
    def __init__(self, h_g, h_l, g, k, s, c, glimpses, show):
        super().__init__()
        D_in = 1800
        #D_in = k * g * g * 3
        self.CNNnetwork = nn.Sequential(
            nn.Conv2d(in_channels=3, out_channels=80, stride=2, kernel_size=10),
            nn.BatchNorm2d(80),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=6, stride=4),

            nn.Conv2d(in_channels=80, out_channels=120, kernel_size=5, stride=1),
            nn.BatchNorm2d(120),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=3, stride=2),

            nn.Conv2d(in_channels=120, out_channels=160, kernel_size=3, stride=1),
            nn.ReLU(),

            nn.Conv2d(in_channels=160, out_channels=200, kernel_size=3, stride=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=3, stride=2), # output: 128 x 12 x 12

            nn.Flatten(),
            nn.Linear(D_in, 320),
            nn.ReLU(inplace=False),
            nn.Dropout(),
            nn.Linear(320, 320),
            nn.ReLU(inplace=False),
            nn.Dropout(),
            nn.Linear(320, h_g+h_l)
        )
        self.Spatial_attn = Spatial_attention(g, k, s, glimpses, show=show)

        D_in = k * g * g * 3
        #D_in = 292032
        #print(D_in)
        self.fc1 = nn.Linear(D_in, h_g)
        # location layer
        D_in = 3
        D_third = 32
        self.fc2 = nn.Linear(D_in, D_third)

        self.fc3 = nn.Linear(D_third ,h_l)

        self.fc4 = nn.Linear(h_l, h_g + h_l)

    def forward(self, x, l_t_prev, step_size):
        phi, start, end, phi_default = self.Spatial_attn.foveate(x, l_t_prev)

        l_t_prev = l_t_prev.view(l_t_prev.size(0), -1)

        l_out = F.relu(self.fc2(l_t_prev))
        l_out = F.relu(self.fc3(l_out))
        where = self.fc4(l_out)
        what = self.CNNnetwork(phi_default)

        g_t = torch.sigmoid(torch.cat((what,  where), 1))
        return g_t, what, start, end

class BaselineNetwork(nn.Module):
    """The baseline network.
    This network regresses the baseline in the
    reward function to reduce the variance of
    the gradient update.
    Args:
        input_size: input size of the fc layer.
        output_size: output size of the fc layer.
        h_t: the hidden state vector of the core network
            for the current time step `t`.
    Returns:
        b_t: a 2D vector of shape (B, 1). The baseline
            for the current time step `t`.
    """

    def __init__(self, input_size, output_size):
        super().__init__()

        self.fc = nn.Linear(input_size, output_size)

    def forward(self, h_t):
        b_t = self.fc(h_t.detach())
        return b_t

class CoreNetwork(nn.Module):
    """The core network.
    The reccurent component of the system. Aggregates information extracted from all
    individual glimpses and their corresponding locations.
    It receives as input the joint spatial and appearance representation (i.e.
    gp) and maintains an internal state summarizing information extracted from the
    sequence of past glimpses. At each step p, the recurrent attention network updates
    its internal state (formed by the hidden units of the network) based on
    the incoming feature representation gp and outputs a prediction for the next
    location lt+1 to focus on at time step t + 1
    Essentially:
        `h_t = relu( fc(h_t_prev) + fc(g_t) )`
    Args:
        input_size: input size of the rnn.
        hidden_size: hidden size of the rnn.
        output_size: output size of the fc layer.
        std: standard deviation of the normal distribution.
        g_t: a 2D tensor of shape (B, hidden_size). The glimpse
            representation returned by the spatial and appearance network for the
            current timestep `t`.
        h_t_prev: a 2D tensor of shape (B, hidden_size). The
            hidden state vector for the previous timestep `t-1`.
    Returns:
        mu: a 2D vector of shape (B, 2).
        l_t: a 2D vector of shape (B, 2).
    """

    def __init__(self, input_size, hidden_size, output_size, std, patch_size):
        super().__init__()
        self.g = patch_size
        self.input_size = input_size
        self.hidden_size = hidden_size

        self.i2h = nn.Linear(hidden_size, hidden_size)
        self.h2h = nn.Linear(hidden_size, hidden_size)

        #next loaction extraction
        self.std = std
        hid_size = input_size // 2
        self.fc = nn.Linear(input_size, hid_size)
        self.fc_lt = nn.Linear(hid_size, 3)


    def forward(self, g_t, h_t_prev, size):
        h1 = self.i2h(g_t)

        h2 = self.h2h(h_t_prev)

        h_t = F.relu(h1+h2)

        feat = F.relu(self.fc(h_t.detach()))

        mu= torch.tanh(self.fc_lt(feat))
        l_t = torch.distributions.Normal(mu, self.std).rsample()
        l_t = l_t.detach()
        log_pi = Normal(mu, self.std).log_prob(l_t)
        log_pi = torch.sum(log_pi, dim=1)

        l_t = torch.clamp(l_t, -1, 1)
        return h_t, l_t, log_pi

class SelectiveExplorationNetwork(nn.Module):
    """The selective epxloration network.
    Given a batch of patches, the network uses the features
    from extracted patches and feeds them through a fully connected
    layer and then applies the sigmoid activation to obtain the probalities
    """

    def __init__(self, output_size):
        super().__init__()
        feat_in = 256
        feat_2_in = feat_in // 2
        self.fc = nn.Linear(feat_in, feat_2_in)
        self.fc2 = nn.Linear(feat_2_in, output_size)

    def forward(self, feat_x):
        x = F.relu(feat_x)
        x = F.relu(self.fc(x))
        prob_t = torch.sigmoid(self.fc2(x))
        return prob_t

class InceptionV3(nn.Module):

    def __init__(self, model_url, num_classes=2, pre_trained=True,
                 aux_logits=False, transform_input=False):
        super(InceptionV3, self).__init__()

        self.add_fc_nclass = False
        self.target_n_classes = num_classes

        if pre_trained:
            self.add_fc_nclass = True
            self.inception_model = tvm.inception_v3(pretrained=pre_trained, aux_logits=aux_logits,
                                                    transform_input=transform_input)
            # Re-init last layer
            self.inception_model.Mixed_7b = InceptionE(1280)
            self.inception_model.Mixed_7c = InceptionE(2048)
            self.inception_model.fc = nn.Linear(2048, self.target_n_classes)

        else:
            self.inception_model = tvm.Inception3(num_classes=num_classes,
                                                  aux_logits=aux_logits,
                                                  transform_input=transform_input)

    def forward(self, x):

        ix = torch.sigmoid(self.inception_model(x))
        return ix

class ActionNetwork(nn.Module):
    """The action network.
    Uses the internal state `h_t` of the core network to
    produce the final output classification.
    Concretely, feeds the hidden state `h_t` through a fc
    layer followed by a softmax to create a vector of
    output probabilities over the possible classes.
    Hence, the environment action `a_t` is drawn from a
    distribution conditioned on an affine transformation
    of the hidden state vector `h_t`, or in other words,
    the action network is simply a linear softmax classifier.
    Args:
        input_size: input size of the fc layer.
        output_size: output size of the fc layer.
        h_t: the hidden state vector of the core network
            for the current time step `t`.
    Returns:
        a_t: output probability vector over the classes.
    """

    def __init__(self, input_size, output_size):
        super().__init__()

        self.fc = nn.Linear(input_size, output_size)

    def forward(self, h_t):

        a_t = self.fc(h_t)
        # a_t = F.softmax(self.fc(h_t))
        return a_t