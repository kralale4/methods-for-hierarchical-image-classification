import torch.nn as nn
import numpy as np
from models.model_parts_grid import Appearance_Spatial_network_CNN
from models.model_parts_grid import CoreNetwork
from models.model_parts_grid import ActionNetwork
from models.model_parts_grid import SelectiveExplorationNetwork
from models.model_parts_grid import InceptionV3
from models.model_parts_grid import BaselineNetwork
from models.model_parts_grid import SpatialNetwork
from utils import get_patch_label

class Recurrent_vis_atn(nn.Module):
    """A Reccurent visual attention model

    Utils:
        Change show to True/False to show tiles with extracted patches
    References:
        https://www2.cs.sfu.ca/~hamarneh/ecopy/miccai2018b.pdf
        https://arxiv.org/abs/1406.6247
    """

    def __init__(
            self, patch_sz, num_patch, scale, chann,
            hid_phi, hid_l, std, hidden_size, num_classes, glimpses
    ):
        super().__init__()
        self.std = std

        self.app_spa_net = SpatialNetwork(hid_phi, hid_l,
                                                    patch_sz, num_patch, scale, chann, glimpses, std)
        self.rnn = CoreNetwork(hidden_size, hidden_size, 2, std, patch_sz)
        self.baseliner = BaselineNetwork(hidden_size, 1)
        self.action_net = ActionNetwork(hidden_size, num_classes)
        self.patch_net = SelectiveExplorationNetwork(num_classes)


    def forward(self, x, l_t_prev, h_t_prev, step_size, last=False):
        g_p, xp, start, end = self.app_spa_net(x, l_t_prev, step_size)
        h_p, l_t, log_pi = self.rnn(g_p, h_t_prev, x.shape)
        patch_prob = self.patch_net(xp)
        b_p = self.baseliner(h_p).squeeze()
        if last:
            log_probas = self.action_net(h_p)
            return h_p, l_t, b_p, log_probas,log_pi, patch_prob, start, end
        return h_p, l_t, b_p, log_pi, patch_prob, start, end
    