import os
import time
import shutil
import pickle
import tensorflow as tf
import torch
import torch.nn.functional as F
import torch.nn as nn
from tqdm import tqdm
from torch.optim.lr_scheduler import ReduceLROnPlateau
from tensorboard_logger import configure, log_value
from torch.utils.tensorboard import SummaryWriter
import matplotlib.pyplot as plt
from numpy import save
import numpy as np
import utils
import utils as ut
from models.recurrent_vis_attn import Recurrent_vis_atn
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

num_channels = 1
num_classes = 2
gamma = 0
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

def train_model(settings, train_gen, val_gen, test_gen=None):
    """
    Trains the reccurent visual attention network on the training dataset and uses the validation dataset
    for evaluation.
    Saves the checkpoints of the model after each epoch.
    :param settings:
    :param train_gen:
    :param val_gen:
    :return:
    """
    global gamma
    if settings['use_gpu'] and torch.cuda.is_available():
        device = torch.device("cuda:0")
    else:
        device = torch.device("cpu")

    model_name = "ram_{}_new_{}x{}_{}_{}".format(
        settings['num_glimpses'],
        settings['patch_size'],
        settings['patch_size'],
        settings['glimpse_scale'],
        "50"
    )
    settings['model_name'] = model_name

    if settings['use_tensorboard']:
        tensorboard_dir = settings['log_path'] + model_name
        print("[*] Saving tensorboard logs to {}".format(tensorboard_dir))
        if not os.path.exists(tensorboard_dir):
            os.makedirs(tensorboard_dir)
        #configure(tensorboard_dir)
        layout = {
            "Reccurent_VAN": {
                "loss": ["Multiline", ["loss/train", "loss/validation"]],
                "accuracy": ["Multiline", ["accuracy/train", "accuracy/validation"]],
            },
        }
        writer = SummaryWriter()
        writer.add_custom_scalars(layout)
    model = Recurrent_vis_atn(
        settings['patch_size'],
        settings['num_patches'],
        settings['glimpse_scale'],
        num_channels,
        settings['loc_hidden'],
        settings['glimpse_hidden'],
        settings['std'],
        settings['hidden_size'],
        num_classes,
        settings['num_glimpses']
    )
    model.to(device)
    #print(model)
    best_valid_acc = 0.0
    best_train_acc = 0.0
    lr_counter = 0
    step = 1/((settings['num_train']/settings['batch_size'])*settings['n_epochs'])
    optimizer = torch.optim.Adam(model.parameters(), lr=settings['init_lr'])
    scheduler = ReduceLROnPlateau(optimizer, "min", patience=settings['lr_reduction'])

    if settings['is_test']:
        model, optimizer, best_valid_acc, start_epoch = load_checkpoint(settings, model, optimizer)
        test_loss, test_acc, model, propabs, Ys = test(test_gen, settings, device, model, step)
        new_propabs = []
        print(Ys)
        print(propabs)
        if not settings['slide_train']:
            propabs = np.asarray(propabs)
            Ys = np.asarray(Ys)

            auc_score = roc_auc_score(Ys, propabs[:, 1])
            save('probs.npy', propabs)
            save('labels.npy', Ys)
            fpr1, tpr1, thresh1 = roc_curve(Ys, propabs[:,1], pos_label=1)
            plt.style.use('seaborn')

    # plot roc curves
            plt.plot(fpr1, tpr1, linestyle='--',color='orange', label='RVAM')
            # title
            plt.title('ROC curve')
            # x label
            plt.xlabel('False Positive Rate')
            # y label
            plt.ylabel('True Positive rate')

            plt.legend(loc='best')
            plt.savefig('roc_curve.png')
            print("\nTest stats\n"
                  "Average test loss: {}, average test acc: {}, test error: {}, auc score: {}\n".format(
                test_loss, test_acc, 100-test_acc, auc_score
            ))
        return propabs, Ys
    start_epoch = 0
    if settings['load']:
        print('loading the most recent state...')
        model, optimizer, best_valid_acc, start_epoch = load_checkpoint(settings, model, optimizer, best=False)
        #optimizer = torch.optim.Adam(model.parameters(), lr=settings['init_lr'])
        print('Most recent state loaded')
    if(start_epoch != 0):
        epoch = start_epoch
    for epoch in range(start_epoch, settings['n_epochs'], 1):

        print("\nEpoch  {} / {} with LR: {}.".format(
            epoch, settings['n_epochs'], optimizer.param_groups[0]["lr"]
        ))

        train_loss, train_acc, optimizer, model, writer = train_one_epoch_supervised(epoch, train_gen, settings, device, model, optimizer, step, writer=writer)
        val_loss, val_acc, model, writer = validate_supervised(epoch, val_gen, settings, device, model, step, writer=writer)
        if best_train_acc < train_acc:
            best_train_acc = train_acc
        else:
            gamma = 0.2
        scheduler.step(-val_acc)
        is_best = val_acc > best_valid_acc
        if(val_acc > best_valid_acc):
            #Improvement measured
            lr_counter = 0
            print("\nEpoch stats\n"
                  "train loss: {}, train acc: {}\n"
                  "valid loss: {}, valid acc: {}\n"
                  "best current validation accuracy\n".format(
                train_loss, train_acc, val_loss, 100-val_acc
            ))
        else:
            #Accuracy has not improved
            lr_counter += 1
            print("\nEpoch stats\n"
                  "train loss: {}, train acc: {}\n"
                  "valid loss: {}, valid acc: {}, valid error: {}\n".format(
                train_loss, train_acc, val_loss,val_acc, 100-val_acc
            ))
        print("*****************************")

        best_valid_acc = max(val_acc, best_valid_acc)
        save_checkpoint(
            {
                "epoch": epoch + 1,
                "model_state": model.state_dict(),
                "optim_state": optimizer.state_dict(),
                "best_valid_acc": best_valid_acc,
            },
            is_best,
            settings=settings
        )
    writer.close()


def load_checkpoint(settings, model, optimizer, best=True,):
    print("[*] Loading model from {}".format(settings['ckpnt_dir']))

    filename = settings['model_name'] + "_ckpt.pth.tar"
    if best:
        print('Loadning the best state...')
        filename = settings['model_name'] + "_model_best.pth.tar"
    ckpt_path = os.path.join(settings['ckpnt_dir'], filename)
    ckpt = torch.load(ckpt_path)

    # load variables from checkpoint
    start_epoch = ckpt["epoch"]
    best_valid_acc = ckpt["best_valid_acc"]
    model.load_state_dict(ckpt["model_state"])
    optimizer.load_state_dict(ckpt["optim_state"])

    if best:
        print(
            "[*] Loaded {} checkpoint @ epoch {} "
            "with best valid acc of {:.3f}".format(
                filename, ckpt["epoch"], ckpt["best_valid_acc"]
            )
        )
    else:
        print("[*] Loaded {} checkpoint @ epoch {}".format(filename, ckpt["epoch"]))
    return model, optimizer, best_valid_acc, start_epoch


def save_checkpoint(state, is_best, settings):
    """Saves a checkpoint of the model.
    If this model has reached the best validation accuracy thus
    far, a seperate file with the suffix `best` is created.
    """
    filename = settings['model_name'] + "_ckpt.pth.tar"
    ckpt_path = os.path.join(settings['ckpnt_dir'], filename)
    torch.save(state, ckpt_path)
    if is_best:
        filename = settings['model_name'] + "_model_best.pth.tar"
        shutil.copyfile(ckpt_path, os.path.join(settings['ckpnt_dir'], filename))

def init_state(batch_size, hidden_size):
    return (torch.zeros(1, batch_size, hidden_size),
            torch.zeros(1, batch_size, hidden_size))

def train_one_epoch_supervised(epoch, train_gen, settings, device, model, optimizer, step, writer=None):
    model.train()
    losses = 0.0
    accs = 0.0
    start_time = time.time()
    #batch_size = settings['batch_size']
    loss_count = 0
    acc_count = 0
    weights_p = torch.tensor([0.2, 0.8]).to(device)
    patch_criterion = nn.CrossEntropyLoss(weight=weights_p)

    slide_criterion = nn.CrossEntropyLoss()
    #slide_criterion = nn.MSELoss().to(device)
    global gamma
    tumor = 0
    with tqdm(total=settings['num_train']) as pbar:
        for i, (Xbatch, Mbatch, tYbatch) in enumerate(train_gen):
            optimizer.zero_grad()

            mask = Mbatch[0]
            batch_size = Xbatch.size()[0]
            plot = True
            Xbatch = Xbatch.to(device)

            tYbatch = tYbatch.to(device)

            imgs = []
            imgs.append(Xbatch[0:9])

            h_t = torch.zeros(
                batch_size,
                settings['hidden_size'],
                dtype=torch.float,
                device=device,
                requires_grad=True,
            )

            l_t = torch.ones(batch_size, 3).to(device)
            l_t[:, 1] = 0
            l_t[:, 2] = 0
            l_t.requires_grad = True

            log_pi = []
            baselines = []
            patch_probs = torch.empty(0, 2).to(device)
            locs = torch.empty((settings['num_glimpses'], batch_size, 3)).to(device)
            starts = []
            ends = []
            patch_labels = []
            for t in range(settings['num_glimpses'] - 1):
                # forward pass through model
                #print('glimpse extractions')
                h_t, l_t, b_t, p, ptch_prob, start, end = model(Xbatch,
                                                    l_t,
                                                    h_t,
                                                    t)
                log_pi.append(p)
                locs[t] = l_t
                baselines.append(b_t)
                #print(ptch_prob)
                patch_probs = torch.cat((patch_probs, ptch_prob), 0)
            h_t, l_t, b_t, log_probas,p, ptch_prob, start, end = model(Xbatch,
                                                            l_t,
                                                            h_t,
                                                            settings['num_glimpses'] - 1,
                                                            last=True)
            log_pi.append(p)
            patch_labels = torch.tensor(patch_labels).to(device).type(torch.int64)
            locs[settings['num_glimpses']-1] = l_t
            baselines.append(b_t)
            patch_probs = torch.cat((patch_probs, ptch_prob), 0)

            predicted = torch.max(log_probas, 1)[1]
            R = (predicted.detach() == tYbatch).float()
            if(batch_size == 1):
                baselines = torch.stack(baselines)
                log_pi = torch.stack(log_pi)
                R = R.unsqueeze(1).repeat(1, settings['num_glimpses'])
                R = R[0]
            else:
                baselines = torch.stack(baselines).transpose(1, 0)
                log_pi = torch.stack(log_pi).transpose(1, 0)
                R = R.unsqueeze(1).repeat(1, settings['num_glimpses'])

            loss_baseline = F.mse_loss(baselines, R)
            adjusted_reward = R - baselines.detach()
            loss_reinforce = torch.sum(-log_pi * adjusted_reward, dim=1)
            loss_reinforce = torch.mean(loss_reinforce, dim=0)

            patch_labels = tYbatch.to('cpu').repeat(settings['num_glimpses']).to(device)

            Lp = utils.Lp_loss(tYbatch, patch_probs, settings, patch_labels, patch_criterion)

            new_probs = torch.empty(batch_size).to(device)

            Lc = slide_criterion(log_probas, tYbatch)

            loss = Lp+Lc+loss_reinforce*0.01

            #compute accuracy
            print(log_probas)
            print(predicted)
            correct = (predicted == tYbatch).float()
            acc = 100 * (correct.sum() / len(tYbatch))

            losses += loss.item()*batch_size
            accs += acc.item()*batch_size
            loss_count += batch_size
            acc_count += batch_size

            # compute gradients and update SGD
            loss.backward()
            optimizer.step()
            end_time = time.time()
            # measure elapsed time
            pbar.set_description(
                (
                    "{:.1f}s - loss: {:.3f} - acc: {:.3f} - gamma: {:3f}".format(
                        (end_time - start_time), loss.item(), acc.item(), gamma
                    )
                )
            )
            pbar.update(batch_size)

                # log to tensorboard
            if settings['use_tensorboard']:
                loss_avg = losses / loss_count
                acc_avg = accs / acc_count
                iteration = epoch * len(train_gen) + i
                writer.add_scalar("loss/train", loss_avg, iteration)
                writer.add_scalar("accuracy/train", acc_avg, iteration)
                writer.flush()
        loss_avg = losses / loss_count
        acc_avg = accs / acc_count
        return loss_avg, acc_avg, optimizer, model, writer

@torch.no_grad()
def validate_supervised(epoch, val_gen, settings, device, model, step, writer=None):
    losses = 0.0
    accs = 0.0
    start_time = time.time()
    loss_count = 0
    acc_count = 0
    patch_criterion = nn.CrossEntropyLoss()
    slide_criterion = nn.CrossEntropyLoss()
    global gamma
    for i, (Xbatch, Mbatch, tYbatch) in enumerate(val_gen):
        mask = Mbatch[0]
        Xbatch = Xbatch.to(device)
        tYbatch = tYbatch.to(device)
        Xbatch = Xbatch.repeat(settings['M'], 1, 1, 1)
        batch_size = Xbatch.shape[0]
        h_t = torch.zeros(
            batch_size,
            settings['hidden_size'],
            dtype=torch.float,
            device=device,
            requires_grad=True,
        )

        l_t = torch.ones(batch_size, 3).to(device)
        l_t[:, 1] = 0
        l_t[:, 2] = 0
        l_t.requires_grad = True

        baselines = []
        patch_probs = torch.empty(0, 2).to(device)
        locs = torch.empty((settings['num_glimpses'], batch_size, 3)).to(device)

        for t in range(settings['num_glimpses'] - 1):
            # forward pass through model
            #print('glimpse extractions')
            h_t, l_t, b_t, p, ptch_prob, start, end = model(Xbatch,
                                                            l_t,
                                                            h_t,
                                                            t)

            locs[t] = l_t
            baselines.append(b_t)
            #print(ptch_prob)
            patch_probs = torch.cat((patch_probs, ptch_prob), 0)
        h_t, l_t, b_t, log_probas,p, ptch_prob, start, end = model(Xbatch,
                                                                   l_t,
                                                                   h_t,
                                                                   settings['num_glimpses'] - 1,
                                                                   last=True)

        patch_labels = tYbatch.to('cpu').repeat(settings['num_glimpses']).to(device)
        locs[settings['num_glimpses']-1] = l_t
        baselines.append(b_t)
        patch_probs = torch.cat((patch_probs, ptch_prob), 0)

        Lp = utils.Lp_loss(tYbatch, patch_probs, settings, patch_labels, patch_criterion)


        # calculate predicted
        predicted = torch.max(log_probas, 1)[1]

        # compute slide level loss Lc
        Lc = slide_criterion(log_probas, tYbatch)


        # sum up into a hybrid loss
        loss = Lp+Lc

        #compute accuracy
        correct = (predicted == tYbatch).float()
        acc = 100 * (correct.sum() / len(tYbatch))

        losses += loss.item()*batch_size
        accs += acc.item()*batch_size
        loss_count += Xbatch.size()[0]
        acc_count += Xbatch.size()[0]

        if settings['use_tensorboard']:
            loss_avg = losses / loss_count
            acc_avg = accs / acc_count
            iteration = epoch * len(val_gen) + i
            writer.add_scalar("loss/validation", loss_avg, iteration)
            writer.add_scalar("accuracy/validation", acc_avg, iteration)
            writer.flush()
    loss_avg = losses / loss_count
    acc_avg = accs / acc_count
    return loss_avg, acc_avg, model, writer

@torch.no_grad()
def test(tst_gen, settings, device, model, step, writer=None):
    losses = 0.0
    accs = 0.0
    start_time = time.time()
    loss_count = 0
    acc_count = 0
    patch_criterion = nn.CrossEntropyLoss()
    slide_criterion = nn.CrossEntropyLoss()
    probs = []
    tumor = 0
    yss = []
    global gamma
    for i, (Xbatch, Mbatch, tYbatch) in enumerate(tst_gen):
        #print(device)
        if tYbatch == 0:
            continue
        print(tYbatch)
        name = 'default'

        utils.save_tensor_images(name, Xbatch.clone().detach())
        Xbatch = Xbatch.to(device)
        tYbatch = tYbatch.to(device)

        batch_size = Xbatch.shape[0]
        h_t = torch.zeros(
            batch_size,
            settings['hidden_size'],
            dtype=torch.float,
            device=device,
            requires_grad=True,
        )

        l_t = torch.ones(batch_size, 3).to(device)
        l_t[:, 1] = 0
        l_t[:, 2] = 0
        l_t.requires_grad = True
        #print(l_t)

        # extract the glimpses

        baselines = []
        patch_probs = torch.empty(0, 2).to(device)
        locs = torch.empty((settings['num_glimpses'], 3))
        starts = []
        ends = []
        patch_labels = []
        for t in range(settings['num_glimpses'] - 1):
            # forward pass through model
            h_t, l_t, b_t, p, ptch_prob, start, end = model(Xbatch,
                                                            l_t,
                                                            h_t,
                                                            t)

            lt = l_t.to('cpu')[0]
            print(lt)
            locs[t] = lt
            baselines.append(b_t)
            #print(ptch_prob)
            patch_probs = torch.cat((patch_probs, ptch_prob), 0)
        h_t, l_t, b_t, log_probas,p, ptch_prob, start, end = model(Xbatch,
                                                                   l_t,
                                                                   h_t,
                                                                   settings['num_glimpses'] - 1,
                                                                   last=True)
        patch_labels = tYbatch.to('cpu').repeat(settings['num_glimpses']).to(device)
        #locs.append(l_t[0:9])
        lt = l_t.to('cpu')[0]
        locs[settings['num_glimpses']-1] = lt

        baselines.append(b_t)
        patch_probs = torch.cat((patch_probs, ptch_prob), 0)

        locs = locs[:, [1, 2]]
        locs = (0.5 * ((locs + 1.0) * 5000)).long()
        ends = locs.clone()

        ends[:, 0] += 50
        ends[:, 1] += 50

        img = utils.render_img(Xbatch[0].clone().to('cpu').numpy(), locs, ends, settings['num_glimpses'], settings['patch_size'], thickness=20, color=(0,0,0))
        print('*****************')

        Lp = utils.Lp_loss(tYbatch, patch_probs, settings, patch_labels, patch_criterion)
        probs = [*probs, *(log_probas.detach().to('cpu').tolist())]
        yss = [*yss, *(tYbatch.detach().to('cpu').tolist())]

        #probs.append(log_probas.detach().to('cpu').tolist()[0])
        # calculate predicted
        predicted = torch.max(log_probas, 1)[1]

        Lc = slide_criterion(log_probas, tYbatch)

        loss = Lp+Lc

        #compute accuracy
        correct = (predicted == tYbatch).float()
        acc = 100 * (correct.sum() / len(tYbatch))

        losses += loss.item()*batch_size
        accs += acc.item()*batch_size
        loss_count += Xbatch.size()[0]
        acc_count += Xbatch.size()[0]

    loss_avg = losses / loss_count
    acc_avg = accs / acc_count
    return loss_avg, acc_avg, model, probs, yss
