import os
import json
import numpy as np
import matplotlib.pyplot as plt
import cv2 as cv
from torchvision import transforms
import matplotlib.patches as patches
import torch.nn.functional as F
from tensorflow.keras.preprocessing.image import save_img
import torch
import tensorflow as tf
import cv2
from kornia.utils.draw import draw_rectangle

pics_dir = '/mnt/home.stud/kralale4/BKP2022/Recurrent_visual_attention_gauss/pics/'

def show_examples(images, labels):
    # X = images.numpy()
    # #X = np.transpose(X, [0, 2, 3, 1])
    # plot_images(X, labels)
    print(np.shape(images))
    X = images.numpy()
    print(np.shape(X))
    X = np.transpose(X, [0, 2, 3, 1])
    plot_images(X, labels)

def La_loss(patch_labels, patch_probs, settings, batch_size, device):

    new_probs = torch.empty(batch_size, settings['num_glimpses']).to(device)
    c = 0
    gl = -1
    for i, item in enumerate(patch_probs):
        if (i%batch_size) == 0:
            c = 0
            gl += 1
        new_probs[c][gl] = item[patch_labels[i]]
        c+=1

    La = torch.zeros(1).to(device)
    for i in range(settings['num_glimpses']):
        if i == 0:
            continue
        La_tmp = new_probs[:, i] - torch.sum(new_probs[:, 0:i], axis=1)/i
        #print(La_tmp)
        La += torch.sum(La_tmp)
    La = -La
    print(La)
    return La

def Lc_loss(log_probas, tYbatch):
    Lc = F.nll_loss(log_probas, tYbatch)
    return Lc

def Lp_loss(tYbatch, patch_probs, settings, patch_labels, patch_criterion):
    print(patch_labels)
    print(patch_probs)
    Lp = patch_criterion(patch_probs, patch_labels)
    return Lp

def Ll_loss(matr_locs, gamma,  settings, batch_size):
    #print(locs)
    locs = matr_locs.squeeze()
    print(matr_locs)
    if(batch_size == 1):
        locs = locs[:, [1, 2]]
        locs_second = locs[1:]
        locs_first = locs[:-1]
        print(torch.sum(torch.pow(locs_first - locs_second, 2), axis=1))
        Ll = gamma * torch.sum(torch.exp(-torch.sqrt(torch.sum(torch.pow(locs_first - locs_second, 2), axis=1))))
    else:
        locs = locs[:, :, [1, 2]]
        locs_second = locs[1:]
        locs_first = locs[:-1]
        Ll = gamma * torch.sum(torch.exp(-torch.sqrt(torch.sum(torch.pow(locs_first - locs_second, 2), axis=2))))
    print(locs)
    # locs_second = locs[1:]
    # locs_first = locs[:-1]
    print(locs_first - locs_second)

    return Ll

def Ll_loss_old(locs, gamma,  settings, batch_size):
    locs_np = np.empty((settings['num_glimpses'], batch_size, 2))
    for i, item in enumerate(locs.copy()):
        locs_np[i] = item.to('cpu').numpy()
    locs_copy = locs_np

    locs_second = locs_copy[1:]
    locs_first = locs_copy[:-1]

    # print(locs_first-locs_second)
    # print( np.exp(-np.sqrt(np.sum(np.power(locs_first-locs_second, 2), axis=2))))
    Ll = gamma * np.sum(np.exp(-np.sqrt(np.sum(np.power(locs_first - locs_second, 2), axis=2))))

def plot_images(images, gd_truth, N0=1):
    """
    :param images: loaded images to be shown
    :param gd_truth: ground truth label of each image
    :param N0: number of images to be shown
    :return:
    """


    fig, axis = plt.subplots(2, 2)
    for i, ax in enumerate(axis.flat):
        print(i)
        ax.imshow(images[i], cmap="Greys_r")
        label = "{}".format(gd_truth[i])
        ax.set_xlabel(label)
        ax.set_xticks([])
        ax.set_yticks([])
        # if i == N0:
        #     break
    print('saving plot')
    plt.savefig('sample.png')

def save_tensor_images(f_name, images):
    images = images.to(torch.device("cpu"))
    for i in range(len(images)):
        X = images[i].permute(1, 2, 0)
        X = X.numpy()
        save_img((pics_dir + f_name+str(i)+'.png'), X)

def save_tensor_image(f_name, image):
    image = image.to(torch.device("cpu"))
    X = image.permute(1, 2, 0)
    X = X.numpy()
    save_img((f_name + '.png'), X)

def count_black_white(mask):
    #print(mask.shape)
    mask = mask.permute(1, 2, 0)
    mask = mask.numpy()
    sought = [1,1,1]
    white  = np.count_nonzero(np.all(mask==sought,axis=2))
    #print(f"white: {white}")

    # Count black pixels
    sought = [0,0,0]
    black  = np.count_nonzero(np.all(mask==sought,axis=2))
    #print(f"black: {black}")
    return white, black

def get_patch_label(mask, start, end, Ybatch):
    B, C, H, W = mask.shape
    #TODO
    # tune the threshold value
    threshold = 0.9
    yt = []
    for i in range(B):
        patch = mask[i, :, start[i, 1] : end[i, 1], start[i, 0] : end[i, 0]]
        C, H, W = patch.shape
        #print(patch.shape)
        white, black = count_black_white(patch)
        white = white/(H*H)
        black = black/(H*H)
        y = 0
        if(white > threshold):
            y = 1
        elif black > threshold:
            y = 0
        else:
            y = int(Ybatch[i].item())
        yt.append(y)

    return yt

def render_img(img_old, start, end, glimpse_num, size, thickness=1, color=(0, 0, 255)):
    img = img_old.copy()
    img = np.transpose(img, [1, 2, 0])

    if thickness > 0:
        for n in range(glimpse_num):
            img = cv2.rectangle(img, (start[n][1], start[n][0]), (end[n][1], end[n][0]), color=color,
                                 thickness=thickness)
    rgb = cv2.UMat.get(img)
    save_img((pics_dir + 'test_render' + '.jpg'), rgb)
    print('saved')
    return img

def show_extracted_patches(images, start, end):
    color = torch.tensor([255, 0, 0])
    x, y = torch.tensor([6, 4]), torch.tensor([1, 4])
    batch_n = np.shape(images)[0]
    rects = torch.tensor([[[start[0, 1], start[0, 0], end[0, 1], end[0, 0]]]])
    for i in range(1, batch_n):
        rect = torch.tensor([[[start[i, 1], start[i, 0], end[i, 1], end[i, 0]]]])
        rects = torch.cat((rects, rect), 0)

    images_with_rect = draw_rectangle(images, rects)
    save_tensor_images('tile_with_rect', images_with_rect)
