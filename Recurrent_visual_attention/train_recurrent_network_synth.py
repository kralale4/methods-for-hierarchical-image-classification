import os
import time
import shutil
import pickle
import tensorflow as tf
import torch
import torch.nn.functional as F
import torch.nn as nn
from tqdm import tqdm
from torch.optim.lr_scheduler import ReduceLROnPlateau
from tensorboard_logger import configure, log_value
import numpy as np

import utils
import utils as ut

from models.recurrent_vis_attn import Recurrent_vis_atn
#from utils import AverageMeter
# device = None
# model = None
# optimizer = None
num_channels = 1
num_classes = 2
gamma = 1

def train_model(settings, train_gen, val_gen):
    """
    Trains the reccurent visual attention network on the training dataset and uses the validation dataset
    for evaluation.
    Saves the checkpoints of the model after each epoch.
    :param settings:
    :param train_gen:
    :param val_gen:
    :return:
    """
    if settings['use_gpu'] and torch.cuda.is_available():
        device = torch.device("cuda:0")
    else:
        device = torch.device("cpu")
    if settings['use_tensorboard']:
        model_name = "ram_{}_{}x{}_{}".format(
            settings['num_glimpses'],
            settings['patch_size'],
            settings['patch_size'],
            settings['glimpse_scale'],
        )
        settings['model_name'] = model_name
        tensorboard_dir = settings['log_path'] + model_name
        print("[*] Saving tensorboard logs to {}".format(tensorboard_dir))
        if not os.path.exists(tensorboard_dir):
            os.makedirs(tensorboard_dir)
        configure(tensorboard_dir)
    model = Recurrent_vis_atn(
        settings['patch_size'],
        settings['num_patches'],
        settings['glimpse_scale'],
        num_channels,
        settings['loc_hidden'],
        settings['glimpse_hidden'],
        settings['std'],
        settings['hidden_size'],
        num_classes,
        settings['num_glimpses']
    )
    model.to(device)
    print(model)
    best_valid_acc = 0.0
    lr_counter = 0
    step = 1/((settings['num_train']/settings['batch_size'])*settings['n_epochs'])
    optimizer = torch.optim.Adam(model.parameters(), lr=settings['init_lr'])
    scheduler = ReduceLROnPlateau(optimizer, "min", patience=settings['lr_reduction'])
    for epoch in range(settings['n_epochs']):
        print("\nEpoch  {} / {} with LR: {}.".format(
            epoch, settings['n_epochs'], optimizer.param_groups[0]["lr"]
        ))

        train_loss, train_acc, optimizer, model = train_one_epoch_supervised(epoch, train_gen, settings, device, model, optimizer, step)
        val_loss, val_acc, model = validate_supervised(epoch, val_gen, settings, device, model, step)

        scheduler.step(-val_acc)

        if(val_acc > best_valid_acc):
            #Improvement measured
            lr_counter = 0
            print("\nEpoch stats\n"
                  "train loss: {}, train acc: {}\n"
                  "valid loss: {}, valid acc: {}\n"
                  "best current validation accuracy\n".format(
                train_loss, train_acc, val_loss, 100-val_acc
            ))
        else:
            #Accuracy has not improved
            lr_counter += 1
            print("\nEpoch stats\n"
                  "train loss: {}, train acc: {}\n"
                  "valid loss: {}, valid acc: {}, valid error: {}\n".format(
                train_loss, train_acc, val_loss,val_acc, 100-val_acc
            ))
        print("*****************************")

        best_valid_acc = max(val_acc, best_valid_acc)
        save_checkpoint(
            {
                "epoch": epoch + 1,
                "model_state": model.state_dict(),
                "optim_state": optimizer.state_dict(),
                "best_valid_acc": best_valid_acc,
            },
            (val_acc > best_valid_acc),
            settings=settings
        )

def save_checkpoint(state, is_best, settings):
    """Saves a checkpoint of the model.
    If this model has reached the best validation accuracy thus
    far, a seperate file with the suffix `best` is created.
    """
    filename = settings['model_name'] + "_ckpt.pth.tar"
    ckpt_path = os.path.join(settings['ckpnt_dir'], filename)
    torch.save(state, ckpt_path)
    if is_best:
        filename = settings['model_name'] + "_model_best.pth.tar"
        shutil.copyfile(ckpt_path, os.path.join(settings['ckpnt_dir'], filename))


def train_one_epoch_supervised(epoch, train_gen, settings, device, model, optimizer, step):
    model.train()
    losses = 0.0
    accs = 0.0
    start_time = time.time()
    #batch_size = settings['batch_size']
    loss_count = 0
    acc_count = 0
    weights_p = torch.tensor([0.2, 0.8]).to(device)
    patch_criterion = nn.CrossEntropyLoss(weight=weights_p)

    slide_criterion = nn.CrossEntropyLoss()
    global gamma
    with tqdm(total=settings['num_train']) as pbar:
        for i, (Xbatch, tYbatch) in enumerate(train_gen):
            optimizer.zero_grad()

            batch_size = Xbatch.size()[0]
            plot = True
            Xbatch = Xbatch.to(device)

            tYbatch = tYbatch.to(device)

            imgs = []
            imgs.append(Xbatch[0:9])
            h_t = torch.zeros(
                batch_size,
                2*settings['hidden_size'],
                dtype=torch.float,
                device=device,
                requires_grad=True,
            )

            print(Xbatch.shape)
            l_t = torch.ones(batch_size, 4).to(device)
            l_t[:, 2] = 0
            l_t[:, 3] = 0
            l_t.requires_grad = True

            l_t = l_t


            # extract the glimpses
            locs = []
            log_pi = []
            baselines = []
            patch_probs = torch.empty(0, 2).to(device)

            log_pi = []

            patch_labels = []
            for t in range(settings['num_glimpses'] - 1):

                h_t, l_t, b_t, p, ptch_prob, start, end = model(Xbatch,
                                                    l_t,
                                                    h_t,
                                                    t)

                log_pi.append(p)
                print(start)

                baselines.append(b_t)

                patch_probs = torch.cat((patch_probs, ptch_prob), 0)
            h_t, l_t, b_t, log_probas,p, ptch_prob, start, end = model(Xbatch,
                                                            l_t,
                                                            h_t,
                                                            settings['num_glimpses'] - 1,
                                                            last=True)

            patch_labels = torch.tensor(patch_labels).to(device).type(torch.int64)

            log_pi.append(p)

            baselines.append(b_t)
            patch_probs = torch.cat((patch_probs, ptch_prob), 0)


            print('*****************')
            print(tYbatch)

            predicted = torch.max(log_probas, 1)[1]


            patch_labels = tYbatch.to('cpu').repeat(settings['num_glimpses']).to(device)

            Lp = utils.Lp_loss(tYbatch, patch_probs, settings, patch_labels, patch_criterion)


            Lc = slide_criterion(log_probas, tYbatch)

            if gamma <= 0:
                gamma = 0
            else:
                gamma -= step

            loss = Lp+Lc


            correct = (predicted == tYbatch).float()
            acc = 100 * (correct.sum() / len(tYbatch))


            losses += loss.item()*batch_size
            accs += acc.item()*batch_size
            loss_count += batch_size
            acc_count += batch_size

            # compute gradients and update SGD
            loss.backward()
            optimizer.step()
            end_time = time.time()
            # measure elapsed time
            pbar.set_description(
                (
                    "{:.1f}s - loss: {:.3f} - acc: {:.3f} - gamma: {:3f}".format(
                        (end_time - start_time), loss.item(), acc.item(), gamma
                    )
                )
            )
            pbar.update(batch_size)


                # log to tensorboard
            if settings['use_tensorboard']:
                loss_avg = losses / loss_count
                acc_avg = accs / acc_count
                iteration = epoch * len(train_gen) + i
                log_value("train_loss", loss_avg, iteration)
                log_value("train_acc", acc_avg, iteration)
        loss_avg = losses / loss_count
        acc_avg = accs / acc_count
        return loss_avg, acc_avg, optimizer, model

@torch.no_grad()
def validate_supervised(epoch, val_gen, settings, device, model, step):
    losses = 0.0
    accs = 0.0
    start_time = time.time()
    loss_count = 0
    acc_count = 0
    weights = torch.tensor([0.2, 0.8]).to(device)
    patch_criterion = nn.CrossEntropyLoss(weight=weights)
    slide_criterion = nn.CrossEntropyLoss()
    global gamma
    for i, (Xbatch, tYbatch) in enumerate(val_gen):
        #print(device)
        Xbatch = Xbatch.to(device)
        tYbatch = tYbatch.to(device)
        Xbatch = Xbatch.repeat(settings['M'], 1, 1, 1)
        batch_size = Xbatch.shape[0]
        h_t = torch.zeros(
            batch_size,
            2*settings['hidden_size'],
            dtype=torch.float,
            device=device,
            requires_grad=True,
        )
        #print(np.shape(h_t))
        initial_M = torch.zeros((batch_size, 2, 3))
        initial_M[:, 0, 0] = 1
        initial_M[:, 1, 1] = 1
        initial_M.requires_grad = True
        l_t = initial_M

        # extract the glimpses
        log_pi = []
        baselines = []
        patch_probs = torch.empty(0, 2).to(device)
        patch_labels = []
        locs = []
        for t in range(settings['num_glimpses'] - 1):
            # forward pass through model
            #print('glimpse extractions')
            h_t, l_t, b_t,p, ptch_prob, start, end = model(Xbatch,
                                                            l_t,
                                                            h_t,
                                                            t)

            baselines.append(b_t)

            patch_probs = torch.cat((patch_probs, ptch_prob), 0)
        h_t, l_t, b_t, log_probas,p, ptch_prob, start, end = model(Xbatch,
                                                                    l_t,
                                                                    h_t,
                                                                    settings['num_glimpses'] - 1,
                                                                    last=True)

        patch_labels = torch.tensor(patch_labels).to(device).type(torch.int64)

        baselines.append(b_t)
        patch_probs = torch.cat((patch_probs, ptch_prob), 0)

        print('*****************')

        patch_labels = tYbatch.to('cpu').repeat(settings['num_glimpses']).to(device)
        Lp = utils.Lp_loss(tYbatch, patch_probs, settings, patch_labels, patch_criterion)


        # calculate predicted
        predicted = torch.max(log_probas, 1)[1]

        # compute slide level loss Lc

        Lc = slide_criterion(log_probas, tYbatch)



        # sum up into a hybrid loss
        loss = Lp+Lc

        #compute accuracy
        correct = (predicted == tYbatch).float()
        acc = 100 * (correct.sum() / len(tYbatch))



        losses += loss.item()*batch_size
        accs += acc.item()*batch_size
        loss_count += Xbatch.size()[0]
        acc_count += Xbatch.size()[0]

        if settings['use_tensorboard']:
            loss_avg = losses / loss_count
            acc_avg = accs / acc_count
            iteration = epoch * len(val_gen) + i
            log_value("valid_loss", loss_avg, iteration)
            log_value("valid_acc", acc_avg, iteration)
    loss_avg = losses / loss_count
    acc_avg = accs / acc_count
    return loss_avg, acc_avg, model
