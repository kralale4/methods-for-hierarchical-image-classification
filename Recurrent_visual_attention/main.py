import os

import torch
import sys
import data_load.data_loader as loader
import data_load.data_loader_synth as synth_loader
import data_load.slide_data_loader as slide_loader
import numpy as np
from utils import show_examples
from train_recurrent_network import train_model
from train_recurrent_network_synth import train_model as train_synth
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
import matplotlib.pyplot as plt
from numpy import save
from tensorboard_logger import configure, log_value
import csv

if __name__ == '__main__':
    run_settings = {
        'input_description_file':'/mnt/medical_temporary/microscopy/attn_testing/training_20x/training_sn_data_fixedpath.txt',
        # 'input_description_file': '/local/temporary/herinjan/training_20x/training_sn_fixedlist.txt',
        #'testing_description_file': '/mnt/medical_temporary/microscopy/attn_testing/testing_20x/testing_data_reduced_fixedpath.txt',
        'testing_description_file': 'None',
        'run_id': 'debug_mlevel_b8_fullTrain',
        'log_path': '/mnt/home.stud/kralale4/BKP2022/logs/RAM',
        'weight_init_dict': '',
        'restore_optimizer': False,
        #'l_criterion': nn.MSELoss(reduction='mean'),
        #'c_criterion': nn.MSELoss(reduction='none'),
        'batch_size': 4,
        'n_epochs': 50,
        'n_save_epoch': 5,
        'log_n_step': 500,
        'eval_n_epochs': 5,
        'slide_train' : False,
        'trace': False,
        'load': False,
        'up_mode': 'upconv',
        'patch_size':304,
        'flimpse_scale':1,
        'random_seed': 1,
        'sampling_size_factor': 1.0,
        'num_patches':1,
        'loc_hidden':128,
        'glimpse_hidden':128,
        'glimpse_scale':1,
        'num_glimpses':6,
        'image_channels': 3,
        'image_size': 5000,
        'initial_image_size': 5000,
        'hidden_size': 256,
        'std': 0.05,
        'M': 1,
        'patch_hidden': 128,
        'g_optim_lr_decay': 1000,
        'momentum': 0.5,
        'init_lr': 3e-4,
        'lr_reduction': 20,
        'use_gpu': True,
        'synth': False,
        'is_test': False,
        'shuffle': True,
        'image_limit': 250,
        'sample_data': False,
        'training_split': 0.8,
        'print_freq': 10,
        'use_tensorboard':True,
        'ckpnt_dir': '/mnt/medical_temporary/microscopy/kralale4/Recurrent_attn_network_POMDP/CKPNTS'
    }
    if len(sys.argv) > 1:
        if loader.load_run_settings_from_dict(sys.argv[1], run_settings):
            print("Failed to update configuration")
            exit()
    if not os.path.exists(run_settings['input_description_file']):
        assert('Training file not found!')

    torch.manual_seed(run_settings['random_seed'])
    kwargs = {}
    if run_settings['use_gpu']:
        torch.cuda.manual_seed(run_settings['random_seed'])
        kwargs = {"num_workers": 1, "pin_memory": True}
    if run_settings['synth']:
        synthetic = True
    else:
        synthetic = False
    if synthetic:
        train_generator,val_generator, test_generator, num_train, num_valid = synth_loader.prepare_data(run_settings['input_description_file'],
                                                                                                  batch_size=run_settings['batch_size'],
                                                                                                  image_file_size=run_settings['image_size'],
                                                                                                  initial_size=run_settings['initial_image_size'],
                                                                                                  split_validation=True, skip_negative=False,
                                                                                                  testing_description_file=run_settings[
                                                                                                      'testing_description_file'],
                                                                                                  training_split=run_settings['training_split'],
                                                                                                  # For debugging - limit number of loaded files
                                                                                                  shuffle=run_settings['shuffle'],
                                                                                                  n_img_limit=run_settings['image_limit'])
    elif run_settings['slide_train'] or run_settings['is_test']:
        if run_settings['is_test'] and not run_settings['slide_train']:
            train_generator,val_generator, test_generator, num_train, num_valid = loader.prepare_data(run_settings['input_description_file'],
                                                                  batch_size=run_settings['batch_size'],
                                                                  image_file_size=run_settings['image_size'],
                                                                  initial_size=run_settings['initial_image_size'],
                                                                  split_validation=True, skip_negative=False,
                                                                  testing_description_file=run_settings[
                                                                      'testing_description_file'],
                                                                  training_split=run_settings['training_split'],
                                                                  # For debugging - limit number of loaded files
                                                                  shuffle=run_settings['shuffle'],
                                                                  n_img_limit=run_settings['image_limit'])
    else:
        train_generator,val_generator, test_generator, num_train, num_valid = loader.prepare_data(run_settings['input_description_file'],
                                                                                                  batch_size=run_settings['batch_size'],
                                                                                                  image_file_size=run_settings['image_size'],
                                                                                                  initial_size=run_settings['initial_image_size'],
                                                                                                  split_validation=True, skip_negative=False,
                                                                                                  testing_description_file=run_settings[
                                                                                                      'testing_description_file'],
                                                                                                  training_split=run_settings['training_split'],
                                                                                                  # For debugging - limit number of loaded files
                                                                                                  shuffle=run_settings['shuffle'],
                                                                                                  n_img_limit=run_settings['image_limit'])

    print('Data loaded, starting training...')
    print('**********************************')
    if synthetic:
        run_settings['num_train'] = num_train
        run_settings['num_valid'] = num_valid
        train_synth(run_settings, train_generator, val_generator)
    elif run_settings['is_test'] and run_settings['slide_train']:
        print('Data loaded, starting testing...')
        tst = []
        with open(os.path.join(run_settings['testing_description_file'], 'test_slides.csv'), 'r', encoding='UTF8') as f:
            csvreader = csv.reader(f)
            for row in csvreader:
                # print(row)
                tst.append(row)
        c = 0
        acc = 0.0
        all_probs = []
        all_labels = []
        for row in tst:
            run_settings['testing_description_file'] = row[0]
            if  not os.path.isdir(run_settings['testing_description_file']):
                continue

            train_generator,val_generator, test_generator, num_train, num_valid = slide_loader.prepare_data(run_settings['input_description_file'],
                                                                                                            batch_size=run_settings['batch_size'],
                                                                                                            image_file_size=run_settings['image_size'],
                                                                                                            initial_size=run_settings['initial_image_size'],
                                                                                                            split_validation=True, skip_negative=False,
                                                                                                            testing_description_file=run_settings[
                                                                                                                'testing_description_file'],
                                                                                                            training_split=run_settings['training_split'],
                                                                                                            # For debugging - limit number of loaded files
                                                                                                            shuffle=run_settings['shuffle'],
                                                                                                            n_img_limit=run_settings['image_limit'])
            run_settings['num_train'] = num_train
            run_settings['num_valid'] = num_valid
            propabs, Ys = train_model(run_settings, train_generator, val_generator, test_gen=test_generator)

            all_probs.append(propabs[0])
            all_labels.append(Ys[0])

            propabs = np.asarray(propabs)
            slide_prob = np.sum(propabs, 0)

            classif = np.argmax(slide_prob)
            #print(classif)
            tmp_acc = (classif == int(row[1])).astype(float)

            acc += tmp_acc
            c += 1
            propabs = np.asarray(propabs)
        Ys = np.asarray(all_labels)
        all_probs = np.asarray(all_probs)
        avg_acc = acc/c


        auc_score = roc_auc_score(Ys, all_probs[:, 1])
        save('probs.npy', all_probs)
        save('labels.npy', all_labels)
        fpr1, tpr1, thresh1 = roc_curve(Ys, all_probs[:,1], pos_label=1)
        plt.style.use('seaborn')

        # plot roc curves
        plt.plot(fpr1, tpr1, linestyle='--',color='orange', label='RVAM')
        # title
        plt.title('ROC curve')
        # x label
        plt.xlabel('False Positive Rate')
        # y label
        plt.ylabel('True Positive rate')

        plt.legend(loc='best')
        plt.savefig('roc_curve.png')
        print('Testing concluded successfully with average accuracy: {} and AUC score: {}'.format(avg_acc, auc_score))
        print('Exiting...')
        print('**********************************')
    elif run_settings['is_test']:
        run_settings['num_train'] = num_train
        run_settings['num_valid'] = num_valid
        propabs, tumor = train_model(run_settings, train_generator, val_generator, test_gen=test_generator)
    else:
        run_settings['num_train'] = num_train
        run_settings['num_valid'] = num_valid
        train_model(run_settings, train_generator, val_generator)
    print('Training concluded successfully')
    print('Exiting...')
    print('**********************************')
    # for Xbatch, Mbatch, tYbatch in val_generator:
    #     if(run_settings['sample_data']):
    #         show_examples(Xbatch, tYbatch)