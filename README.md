# Methods for hierarchical image classification

# Introduction

In this repository, one can find implemented methods for hierarchical image classification of histology images.
There are two implemented methods to deal with the task. 

Implementation of the Quadtree approach introduced by Jewsbury et. al.

Jewsbury, R., Bhalerao, A., Rajpoot, and N.M. A quadtree image repre-
sentation for computational pathology. In Proceedings of the IEEE/CVF
International Conference on Computer Vision, pages 648–656, 2021

Implementation of the Recurrent Visual Attention Model introduced by BenTaieb et. al.

BenTaieb, Aïcha, , and Ghassan Hamarneh. Predicting cancer with a
recurrent visual attention model for histopathology images. In Proc. of
MICCAI 2018, pages 127–137, 2018.

Methods have been modified to achieve better results in the problem of distinguishing between tumorours and normal tissue.
## Dataset
In experiments, I used the dataset of WSIs publicly available from the [CAMELYON16](https://camelyon16.grand-challenge.org/) challenge

First a user needs to prepare a dataset of tiles covering the WSI foreground.
This is best done using the [Histomicstk](https://digitalslidearchive.github.io/HistomicsTK/) library or the [PyHist](https://pyhist.readthedocs.io/en/latest/tutorial/) library.

Example of the extraction of 5000x5000 pixels large tiles from a slide using the PyHist library
```
python pyhist.py --save-patches --info "verbose" --save-tilecrossed-image --borders 0000 --corners 1010 --percentage-bc 1 --content-threshold 0.6 --patch-size 5000 --output-downsample 3 --k-const 1000 --save-patches --save-tilecrossed-image --output [OUTPUT DIRECTORY] [INPUT SLIDE]
```
## Configuration files
The behaviour of the methods is specified through the configuration files. Those are different for each method. A user can create their own config files or use the templates available in the configs/ and qnfigs/ directories.

# Quadtree
This implementation creates a quadtree according to its input tiles and parameters. It extracts patches of fixed size and saves them in a user specified directory. Then a modified pretrained ResNet50 network extracts feature vectors from these patches and a Clustering-constrained attention multiples instance learning network is trained on them to produce binary predictions.

## Quadtree pipeline
The first step in the pipeline is to calculate the mean and standard deviation parameters, which make up the splitting threshold.

### Getting mean and std

This can be done by calling the main.py script with the appropriate configuration file. In the following template example, one needs to specify the "input_description_file" containing rows with tiles.
```
python Quadtree_impl/main.py Quadtree_impl/qnfigs/configuration_template_get_mean_std.xml
```
this creates a human readable .txt file, which contains the mean and std calculated from all input tiles.

### Quadtree segmentation

The Quadtree segmentation is then initialized by calling the main.py script again, only with a different configuraiton, where a user needs to copy and paste the mean and std from the output mean_std.txt file.

```
python Quadtree_impl/main.py Quadtree_impl/qnfigs/configuration_template_segment.xml
```

The above call extracts patches from input tiles at a user specified location and a specified patch size. Also patches_train.csv and patches_val.csv files are created to separate the training and validation tiles.

### Quadtree training
Then the training routine can be started by creating a new configuration file and specifying the segmentation_results folder where the patches where extracted. This loads the training and validation datasets. GPU or CPU training can be specified or if one wishes to produce logs of the training "use_tensorboard" parameter needs to be set to True. CUDA devices also need to be specified in the call.

```
CUDA_VISIBLE_DEVICES=0 python Quadtree_impl/main.py Quadtree_impl/qnfigs/configuration_template_train_1_5.xml
```

The above call produces checkpoints of the model at a specified directory after every epoch and also saves the best performing model with the prefix _best.
If the training is interrupted at any point, the "load" parameter can be set to True to continue training from the last checkpoint.
### Quadtree testing
The tests can be performed on tiles or entire slides. The parameter "test"=True parameter is mandatory for all testing, but if the "slide_train"=True parameter is specified along with the "test", then testing on slides is initialized.
The output of the testing is the average accuracy and AUC. Probabilities and labels from the testing are automatically saved to .npy binary files to be used in experiments. A ROC curve from the testing is a saved in the root directory (as .png file).

```
CUDA_VISIBLE_DEVICES=0 python Quadtree_impl/main.py Quadtree_impl/qnfigs/configuration_template_test_1_5.xml
```

# Recurrent visual attention model

This implementation trains the recurrent network on tiles of size 5000x5000 pixels or less. I experimented on tiles of size 8000x8000 pixels, but was unable to process data this large.

### RVAM training
The model is trained on tiles specified by the "input_description_file" in the configuration. Many finetuning parameters can be set, ranging from the extracted "patch_size" to the "num_glimpses" parameter, which specifies the number of glimpses extracted from each tile. The CUDA devices will also need to be specified if the routine should be run on GPU.

```
CUDA_VISIBLE_DEVICES=0 python Recurrent_visual_attention/main.py Recurrent_visual_attention/configs/configuration_template_Lp_Lc_loss.xml
```

### RVAM testing

Testing configuration file uses the same parameters as the testing file in the Quadtree implementation. "test"=True is mandatory, but "slide_train"=True turns on testing of entire slides.

```
CUDA_VISIBLE_DEVICES=0 python Recurrent_visual_attention/main.py Recurrent_visual_attention/configs/configuration_template_Lp_Lc_test.xml
```

The output, after running the above call, is the average accuracy and AUC of the testing. Additional parts of the output include the saved probabilities and labels to .npy binary files for further analysis and a plotted ROC curve in a .png file.
